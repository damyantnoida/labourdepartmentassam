function calculatePremium() {

    var size = ($('input[name="sumInsured"]').length - 1)
    var prevValue = 0, grossPremium = 0
    for (var i = 0; i < $('input[name="premium"]').length; i++) {
        var sumInsured = $('#sumInsured' + i).val()
        var rate = $('#enterRate' + i).val()
        if ($('#sumInsured' + i).val().length > 0 && $('#enterRate' + i).val().length > 0) {
            $('#sumInsured' + i).removeClass('alert alert-info')
            var premium = parseFloat(sumInsured) * (parseFloat(rate) / 100)
            $('#premium' + i).val(parseFloat(premium).toFixed(4))
        }
        else {
            $('#enterRate' + i).val('')
            $('#sumInsured' + i).addClass('alert alert-info')
        }
        grossPremium = parseFloat(prevValue) + parseFloat($('input[name="premium"]').eq(i).val())
        prevValue = $('input[name="premium"]').eq(i).val()
        $('#totalPremium').val(grossPremium.toFixed(4))
    }
}
function addMorePremiums() {
    var status = true
    for (var i = 0; i < $('input[name="sumInsured"]').length; i++) {
        if ($('#sumInsured' + i).val().length == 0) {
            $('#sumInsured' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#sumInsured' + i).removeClass('alert alert-danger')
        }
        if ($('#enterRate' + i).val().length == 0) {
            $('#enterRate' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#enterRate' + i).removeClass('alert alert-danger')
        }
        if ($('#premium' + i).val().length == 0) {
            $('#premium' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#premium' + i).removeClass('alert alert-danger')
        }
        if ($('#description' + i).val().length == 0) {
            $('#description' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#description' + i).removeClass('alert alert-danger')
        }

    }
    if (status == true) {
        var size = ($('input[name="sumInsured"]').length - 1)
        var nextID = (size + 1)
        $('#premiumGroup' + size).after('<div class="form-group" id="premiumGroup' + nextID + '"><div class="col-sm-4 text-center">' +
            '<input type="text" required="" class="form-control" name="description" id="description' + nextID + '"' +
            'placeholder="Enter Description" value=""/></div><div class="col-sm-2 text-center">' +
            '<input type="text" required="" class="form-control" id="sumInsured' + nextID + '" name="sumInsured" placeholder="Enter Sum Insured" value=""/>' +
            '</div><div class="col-sm-2 text-center"><input type="text" required="" class="form-control" name="enterRate" id="enterRate' + nextID + '"' +
            'placeholder="Enter Rate" onchange="calculatePremium(this)" value=""/></div><div class="col-sm-2 text-center">' +
            '<input type="text" required="" readonly class="form-control" name="premium" id="premium' + nextID + '"' +
            ' placeholder="Premium" value=""/></div><div class="col-sm-2 text-center"></div></div>')

    }
}

function calculateDiscountAmount() {
    var prevValue = 0, grossPremium = 0
    for (var i = 0; i < $('input[name="addLessTotalDiscountRate"]').length; i++) {
        var payableAmount= 0, discountedAmount=0
        var premiumAmount=0
        if($('#totalPremium').val().length>0){
            $('#totalPremium').removeClass('alert alert-danger')
            if(i==0){
                premiumAmount=$('#totalPremium').val()
            }
            else{
                premiumAmount=$('#totalPayableAmount').val()
            }
            if($('#addLessTotalPremium' + i).val().length){
                $('#addLessTotalPremium' + i).removeClass('alert alert-danger')
                if($('#addLessTotalDiscountRate' + i).val().length){
                    $('#addLessTotalDiscountRate' + i).removeClass('alert alert-danger')
                    var addOrLess = $('#addLessTotalPremium' + i).val()
                    var rate = $('#addLessTotalDiscountRate' + i).val()
                    discountedAmount=parseFloat(premiumAmount)*(parseFloat(rate)/100)
                    if(addOrLess=='+'){
                        payableAmount=parseFloat(premiumAmount) + parseFloat(discountedAmount)
                    }
                    else if(addOrLess=='-'){
                        payableAmount=parseFloat(premiumAmount) - parseFloat(discountedAmount)
                    }
                    $('#addLessTotalDiscountValue' + i).val(discountedAmount.toFixed(4))
                    $('#totalPayableAmount').val(payableAmount.toFixed(4))
                }
                else{
                    $('#addLessTotalDiscountRate' + i).addClass('alert alert-danger')
                }

            }
            else{
                $('#addLessTotalPremium' + i).addClass('alert alert-danger')
            }


        }
        else{
            $('#totalPremium').addClass('alert alert-danger')
        }

    }
}
function addMoreDiscountPremium() {
    var status = true
    for (var i = 0; i < $('input[name="addLessTotalDiscountRate"]').length; i++) {
        if ($('#addLessTotalDiscountLabel' + i).val().length == 0) {
            $('#addLessTotalDiscountLabel' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#addLessTotalDiscountLabel' + i).removeClass('alert alert-danger')
        }
        if ($('#addLessTotalPremium' + i).val().length == 0) {
            $('#addLessTotalPremium' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#addLessTotalPremium' + i).removeClass('alert alert-danger')
        }
        if ($('#addLessTotalDiscountRate' + i).val().length == 0) {
            $('#addLessTotalDiscountRate' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#addLessTotalDiscountRate' + i).removeClass('alert alert-danger')
        }
        if ($('#addLessTotalDiscountValue' + i).val().length == 0) {
            $('#addLessTotalDiscountValue' + i).addClass('alert alert-danger')
            status = false
        }
        else {
            $('#addLessTotalDiscountValue' + i).removeClass('alert alert-danger')
        }

    }
    if (status == true) {
        var size = ($('input[name="addLessTotalDiscountRate"]').length - 1)
        var nextID = (size + 1)
        $('#netPremiumGroup' + size).after('<div class="form-group" id="netPremiumGroup' + nextID + '"><div class="col-sm-4 text-center">' +
            '<input type="text" class="form-control" name="addLessTotalDiscountLabel" id="addLessTotalDiscountLabel' + nextID + '" ' +
            'placeholder="Enter Description" value=""/></div><div class="col-sm-2 text-center dropdown">' +
            '<select name="addLessTotalPremium" id="addLessTotalPremium' + nextID + '" class="btn btn-default dropdown-toggle">' +
            '<option value="">Select Add/Less</option><option value="+">Add</option><option value="-">Less</option>' +
            '</select></div><div class="col-sm-2 text-center"><input type="text" class="form-control" name="addLessTotalDiscountRate" ' +
            'id="addLessTotalDiscountRate' + nextID + '" onchange="calculateDiscountAmount()" placeholder="Enter The Rate" value=""/>' +
            '</div><div class="col-sm-2 text-center"><input type="text" required="" readonly class="form-control" ' +
            'name="addLessTotalDiscountValue" id="addLessTotalDiscountValue' + nextID + '" placeholder="" value=""/></div>' +
            '<div class="col-sm-2 text-center"></div></div>')

    }
}
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function calculateMonthsDifference(){
    d1 = new Date($( "#insuredFromDate" ).val());
    d2 = new Date($( "#insuredToDate").val());
    $('#insuredPeriodInMonths').val(monthDiff(d1, d2)+" Months");
    $('#insuredPeriodInMonths1').val(monthDiff(d1, d2));
}