/**
 * Created by chandan on 3/12/14.
 */
function validate() {
    $("#studyMaterialPage,#bankForm,#addCoursesFrmId,#marksTypeForm,#tempEnrollment,#districtForm,#addNewFeeType,#uploadInternalMarks,#rollNoGenerationDate,#saveExaminationCentre,#createStudyCenter,#individualDownloadAdmitCard,#studentRegister,#createCourse,#generateFeeVoucher,#generateExamFeeVoucher, #createFeeDetail").validate({
        rules: {
            //Add Course
            bankName:"required",
            marksTypeName:"required",
            programTypeId:"required",
            imageValidate:"required",
            subjectName: {required: true
            },

            subjectCode:{required:true},
            aliasCode:{required:true},
            creditPoints:{required:true},
            //theoryMarks:{required:true},
           // homeAssignmentMarks:{required:true},
            totalMarks:{required:true},

            registrationNo1:{
                minlength:5
            },
            registrationNo2:{
                minlength:5
            },
            //studyMaterialPageFields
            studyMaterialText: {
                required: true,
                number: true
            },
//              tempEnroll
            examCentre:"required",
            address:"required",
            examVenue:"required",
//            Study Center
            name: {
                required: true,
                textonly: true
            },
            uploadSyllabus: {
                extension: "doc|pdf|docx"
            },
            programList: "required",
            internalMarks: {
                required:true,
                accept:'xls|xlsx|cvs'
            },
            district: "required",
            city: "required",
            regNoCheck: "required",
            workDistrict: "required",
            districtName: "required",
            parentsName:"required",
            studentAddress:"required",
            addressTown:"required",
            addressPO:"required",
            addressDistrict:"required",

            addressState:"required",
            addressPinCode: "required",
            semesterList: "required",
            examDistrict:"required",
            examCity:"required",
            examinationCentre:"required",

            centerCode: {
                required: true,
                alphanumeric: true
            },
            nameOfHeadIns: {
                required: true,
                textonly: true
            },
            emailIdOfHeadIns: {
                required: true,
                email: true
            },
            nameOfCoordinator: {
                required: true

            },
            emailIdOfCoordinator: {
                required: true,
                email: true
            },
            bankName:{
                required: true
            },
            websiteUrl: {
                required: true,
                url: true
            },
            phoneNoOfCoordinator: {
                required: true,
                minlength: 10,
                number: true
            },
            phoneNoOfHeadIns: {
                required: true,
                minlength: 10,
                number: true
            },
//            asstCoordinator: {
//                required: true
//            },
//            asstMobile: {
//                required: true,
//                minlength: 10,
//                number: true
//            },
//            asstEmail: {
//                required: true,
//                email: true
//            },

//            Study Center

//            Student Enroll

            programDetail: "required",

            nameOfApplicant: {
                required: true
            },
            programId: {
                required: true
            },
            category: {
                required: true
            },
            nationality: {
                required: true
            },
            gender: {
                required: true
            },
            state: {
                required: true
            },
            contactNo: {
                required: true,
                number: true
            },
            contactCentre: {
                required: true
            },
            location: {
                required: true
            },
            firstName: {
                required: true,
                textonly: true
            },
            lastName: {
                required: true,
                textonly: true
            },
            feeReferenceNumber:{

            },
//
            declaration: {
                required: true
            },
            courseName: {
                required: true
            },
            admissionFeeAmount:{
                required: true,
                min: 1
            },
            courseMode: {
                required: true
            },
            courseType: {
                required: true
            },
            noOfTerms: {
                required: true,
                number: true
            },
            courseCode: {
                required: true,
                number: true
            },
            noOfAcademicYears: {
                required: true,
                number: true
            },
//            totalMarks: {
//                required: true,
//                number: true,
//                min:1
//            },
            noOfPapers:  {
                required: true,
                number: true,
                min:1
            },
            marksPerPaper:  {
                required: true,
                number: true,
                min:1
            },
            totalCreditPoints:  {
                required: true,
                number: true,
                min:1
            },

            d_o_b: {
                required: true,
                universityDateFormat: true,
                minlength:10
            },

            startD: {
                required: true
            },
            endD: {
                required: true
            },
            mobileNo: {
                required: true,
                number: true,
                minlength: 10
            },


            studyCentre: "required",

            examiNationCentre: "required",

//              Student Enroll End

//            Exam Center Create
            examinationCentreName: {
                required: true,
                textonly: true
            },


//            CreateNewFeeType
            feeAmountAtIDOL: {
                required: true,
                number: true
            },
            feeAmountAtSC: {
                required: true,
                number: true
            },
            lateFeeAmount: {
                required: true,
                number: true,
                min: 0
            },
            examinationFee: {
                required: true,
                number: true
            },
            certificateFee: {
                required: true,
                number: true
            },
//            fee voucher
            rollNo: {
                required: true,
                number: true,
                minlength: 8
            },
            applicationNo: {
                required: true,
                number: true,
                minlength: 5,
                maxlength: 10
            },
            feeType: {
                required: true
            },
            paymentMode: {
                required: true
            },
            feeReferenceNumber:{
                required: true,
                number: true,
                minlength:6,
                maxlength:8
            },
            draftNumber: {
                required: true,
                number: true
            },
            paymentDate: {
                required: true,
                minlength:10
            },
            draftDate: {
                required: true
            },
            issuingBank: {
                required: true
            },
            issuingBranch: {
                required: true
            },
//              Download Admit Card
            rollNumber:{
                required:true,
                minlength: 8
            },
            branchName:{
                required:true
            },
            dob:{
                required:true
//                date: true
            },
            categoryName:{
                required:true
            },

            //Exam Centre
//            examinationCentreName: {required: true, textonly: true},
            examinationCentreCode: {required: true,
                number: true},
            examinationCentreCapacity: {required: true,
                number: true},
            examinationCentreIncharge: {required: true,
                textonly: true},
            examinationCentreContactNo: {required: true,
                number: true},
            examinationCentreAddress: {required: true},
            examCentreName: {required: true,
                textonly: true},
            type:{
                required: true,
                textonly: true
            }

        },
        messages: {

        },
        errorPlacement: function (error, element) {
            if (element.is("input:radio")) {
                element.parents(".radio_options").after(error);
            } else if (element.is("input:checkbox")) {
                element.parents("#declaration-label").after(error);
            } else {
                element.after(error);
            }
        }


    });
    $.validator.addMethod("universityDateFormat",function (value, element) {
            return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
        },
        "Please enter a date in the format dd/mm/yy"
    );

    jQuery.validator.addMethod("textonly", function (value, element) {
            valid = false;
            check = /[^-\.a-zA-Z\s\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02AE]/.test(value);
            if (check == false)
                valid = true;
            return this.optional(element) || valid;
        },

//        $.validator.addMethod('filesize', function(value, element, param) {
//            // param = size (en bytes)
//            // element = element to validate (<input>)
//            // value = value of the element (file name)
//            return this.optional(element) || (element.files[0].size <= 50)
//        }),



    jQuery.format("Please only enter letters, spaces, periods, or hyphens.")
    );
    jQuery.validator.addMethod("lettersnumberswithbasicpunc", function(value, element) {
        return this.optional(element) || /^[a-z0-9-.,\\:()'\"\s]+$/i.test(value);
    }, "Letters or punctuation only please");

    $.validator.addMethod('minStrict', function (value, element, param) {
            return value > param;
        },
        $.format("Please larger value.")
    );
    $.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");


    jQuery.validator.addMethod("extension", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, jQuery.format("Please enter a value with a valid extension."));
}


function isNumber(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 47 && charCode < 58) || charCode == 9 ||charCode == 11 ||charCode == 8) {
        return true;
    }
    return false;
}

function isMarks(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 47 && charCode < 58) || charCode == 9 ||charCode == 11 ||charCode == 8||charCode == 65||charCode == 66||charCode == 83||charCode == 97||charCode == 98||charCode == 115) {
        return true;
    }
    return false;
}
function isNumberWithDash(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 47 && charCode < 58) || charCode == 9 ||charCode == 11 ||charCode == 8 || charCode == 45) {
        return true;
    }
    return false;
}
function isNumberWithSpaceComma(evt) {

    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 47 && charCode < 58) || charCode == 9 ||charCode == 11 ||charCode == 8 || charCode == 45|| charCode == 32 || charCode == 44) {
        return true;
    }
    return false;
}

function onlyAlphabets(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ( charCode == 11 ||charCode == 9 ||charCode == 8 || charCode == 32 || charCode == 46 || charCode == 45 || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
    {
        return true;
    }
    return false;
}


function isAlphaNumeric(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode == 32 ||charCode == 9 ||charCode == 11||charCode == 8 || (charCode > 47 && charCode < 58) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))) {
        return true;
    }
    return false;
}

function isTime(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode == 9 ||charCode == 58 || charCode == 11 ||charCode == 8 || charCode == 32 || ( charCode > 47 && charCode < 58) || charCode == 65 || charCode == 97 || charCode == 77 || charCode == 109 || charCode == 80 || charCode == 112)) {
        return true;
    }
    return false;
}

function onlyAlphabetsWithSplChar(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 9 ||charCode == 11 ||charCode == 8 ||( charCode > 32 && charCode < 47 )|| ( charCode > 64 && charCode < 91 ) || ( charCode > 96 && charCode < 123 ) )
        return true;
    else
        return false;
}




function disableKeyInput(t){
    $(t).keydown(false);
}
//function disableThisButton(t){
//    $(t).attr("disabled", true)
//}
