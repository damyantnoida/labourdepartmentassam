function generateIdentity(t) {
    var studentId = t;
    $.ajax({
        type: "post",
        url: url('employeeQRGenerate', 'loadStudentInfo', ''),
        data: {studentId: studentId},
        success: function (data) {
            $('#qrCode').empty().append("")
            $('#nameContent').empty().append("" + data.studentInstance.firstName + " " + data.studentInstance.lastName)
            $('#addressContent').empty().append("" + data.studentInstance.address + ", " + data.studentInstance.addressCity + ", <br/>" + data.studentInstance.addressDistrict + ", " + data.studentInstance.addressPinCode)
            $('#aadharContent').empty().append("" + data.studentInstance.identificationNo)
            $('#contectContent').empty().append("" + data.studentInstance.mobileNo)
            $('#dobContent').empty().append("" + data.date)
            document.getElementById("picture").src = "../studentImage/" + data.regYear + "/" + data.programCode + "/" + data.imageName
            var ip = location.host;
            new QRCode(document.getElementById("qrCode"), "http://"+ip+"/labourDepartmentAssam/employeeQRGenerate/qrUpdate?studentId=" + data.studentInstance.id);
        }
    })
    $('#Generate').click()

}

function readURL(input, type) {
    $('#imageValidate').val("")
    if (input.files && input.files[0]) {
        var FileUploadPath = $("#profileImage").val()
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        var imgkbytes = Math.round(parseInt(input.files[0].size) / 1024)
        if (imgkbytes <= 50 && (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg")) {
            var reader = new FileReader();
            if (type == 'picture')
                reader.onload = function (e) {
                    $('#picture')
                        .attr('src', e.target.result)
                        .width(100)
                        .height(130);
                };
            if (type == 'picture1')
                reader.onload = function (e) {
                    $('#picture1')
                        .attr('src', e.target.result)
                        .width(100)
                        .height(130);
                };
            if (type == 'picture2')
                reader.onload = function (e) {
                    $('#picture2')
                        .attr('src', e.target.result)
                        .width(100)
                        .height(130);
                };
            if (type == 'signature')
                reader.onload = function (e) {
                    $('#signature')
                        .attr('src', e.target.result)
                        .width(100)
                        .height(130);
                };
            if ($('#imageValidate').length > 0) {
                $('#imageValidate').val("uploded")
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            $('#imageValidate').val("")
            $("#profileImage").val('')
            $("#picture").attr('src', ' ')
            $("<div></div>").html("<div style='text-align: justify;font-size: 12px;'><p>Please upload an image of size less then 50kb and image Extension should be gif/png/bmp/jpeg/jpg.</p></div>").dialog({
                title: "Sorry",
                resizable: false,
                modal: true,
                buttons: {
                    "Ok": function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    }
}

function loadAgeGraph() {
    $.ajax({
        type: "post",
        url: url('employeeQRGenerate', 'ageGroupGraph', ''),
        data: {},
        success: function (data) {
            $('#myModal').modal('show')
            drawBarChart(data);
        }
    })
    $('#Generate').click()
}
function drawBarChart(data) {
    var finalMap = JSON.parse(data.replace(/&quot;/g, '"'));
    $('#graph').highcharts({
        chart: {
            renderTo: 'container',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        title: {
            text: 'Bar Chart for no. Of workers under different age groups'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: finalMap[0]
            ,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Employee'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">No.of Employee: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Age Group',
            colorByPoint: true,
            data: finalMap[1]
        }]
    });
}
function drawYearBarChart(data) {
    var finalMap = JSON.parse(data.replace(/&quot;/g, '"'));
    $('#graph').highcharts({
        chart: {
            renderTo: 'container',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        title: {
            text: 'Bar Chart for year wise comparison of total registration'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: finalMap[0]
            ,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Employee'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">No.of Employee: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Year',
            colorByPoint: true,
            data: finalMap[1]
        }]
    });
}
function drawGenderPieChart(data) {
    var finalMap = JSON.parse(data.replace(/&quot;/g, '"'));
    $('#graph').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Gender wise Comparison'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Gender share',
            data: finalMap
        }]
    });
}
function drawDistrictPieChart(data) {
    var finalMap = JSON.parse(data.replace(/&quot;/g, '"'));
    $('#graph').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'District wise Worker Comparison'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Worker share',
            data: finalMap
        }]
    });
}


function addMoreSocial(){
   var selectIndex=$(".socialSecuritySelect").length
    var $options = $("#socialSecurity0 > option").clone();
    $('#socialSecurityDiv').append("<select name='socialSecurity' id='socialSecurity"+selectIndex+"'  style='margin-bottom: 3px;' class='socialSecuritySelect col-sm-7 form-control'></select")
    $('#socialSecurity'+selectIndex).append($options);
}
function removeSocial(){
    var selectIndex=$(".socialSecuritySelect").length
    if(selectIndex>1){
        $('.socialSecuritySelect').last().remove();
    }
    else{
        alert("Can not Delete")
    }

}
function showSocialSecurityDetails(t){
    var socialSecurityId = t;
    $.ajax({
        type: "post",
        url: url('employeeQRGenerate', 'loadSocialSecurityDetails', ''),
        data: {socialSecurityId:socialSecurityId},
        success: function (data) {
            $("#schemeHeader").text(data.name)
            $("#benefitAmount").text(data.monthlyAmount)
            $("#trnansferredAmount").text(data.fyAmount)
        }
    })
    $('#popup').bPopup({
        speed: 650,
        transition: 'slideIn',
        transitionClose: 'slideBack'
    });
}
function loadDrillDownDetails(t){
    var socialSecurityId = t;
    $.ajax({
        type: "post",
        url: url('employeeQRGenerate', 'loadDistrictsOfASocialSecurityDetails', ''),
        data: {socialSecurityId:socialSecurityId},
        success: function (data) {
            $("#schemeHeader").empty().text(data.socialSecName)
            $('#mainPopupContent').empty().append(" <div class='col-sm-12' style='margin: auto;padding: 5px 0px;font-size: 15px;font-weight: bolder;'><div class='col-lg-6 text-center'>District</div><div class='col-lg-6 text-center'>Amount Paid</div></div>")

            for (var i=0; i<data.districtList.length;i++){
               $('#mainPopupContent').append(" <div class='col-sm-12' style='margin: auto;padding: 3px 0px;'><div class='col-lg-6 text-center'>"+data.districtList[i] +"</div><div class='col-lg-6 text-center'>&#8377;"+data.amountList[i] +"</div></div>")
            }
        }
    })
    $('#popup').bPopup({
        speed: 650,
        transition: 'slideIn',
        transitionClose: 'slideBack'
    });
}

function loadDrillDownDistrictDetails(t){
    var district = t
    $.ajax({
        type: "post",
        url: url('employeeQRGenerate', 'loadSocialSecurityDetailsOfADistrict', ''),
        data: {district:district},
        success: function (data) {
            $("#schemeHeader").empty().text(district)
            $('#mainPopupContent').empty().append(" <div class='col-sm-12' style='margin: auto;padding: 5px 0px;font-size: 15px;font-weight: bolder;'><div class='col-lg-6 text-center'>Scheme Name</div><div class='col-lg-6 text-center'>Amount Paid</div></div>")

            for (var i=0; i<data.schemeNameList.length;i++){
               $('#mainPopupContent').append(" <div class='col-sm-12' style='margin: auto;padding: 3px 0px;'><div class='col-lg-6 text-center'>"+ data.schemeNameList[i].name +"</div><div class='col-lg-6 text-center'>&#8377; "+data.amountList[i] +"</div></div>")
            }
        }
    })
    $('#popup').bPopup({
        speed: 650,
        transition: 'slideIn',
        transitionClose: 'slideBack'
    });
}