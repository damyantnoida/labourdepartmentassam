import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_labourDepartmentAssam_employeeQRGeneratereport_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/employeeQRGenerate/report.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',10,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
createTagBody(2, {->
invokeTag('captureTitle','sitemesh',11,[:],-1)
})
invokeTag('wrapTitleTag','sitemesh',11,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir: 'js', file: 'myJquery.js'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js/Highcharts-4.1.7/js', file: 'highcharts.js'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js/Highcharts-4.1.7/js', file: 'highcharts-3d.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'js/Highcharts-4.1.7/js/modules', file: 'exporting.js'))
printHtmlPart(6)
})
invokeTag('captureHead','sitemesh',19,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',23,['controller':("employeeQRGenerate"),'action':("ageGroupGraph"),'target':("_self")],2)
printHtmlPart(9)
createClosureForHtmlPart(10, 2)
invokeTag('link','g',29,['controller':("employeeQRGenerate"),'action':("yearWiseRegistration"),'target':("_self")],2)
printHtmlPart(11)
createClosureForHtmlPart(12, 2)
invokeTag('link','g',32,['controller':("employeeQRGenerate"),'action':("genderComparison"),'target':("_self")],2)
printHtmlPart(11)
createClosureForHtmlPart(13, 2)
invokeTag('link','g',33,['controller':("employeeQRGenerate"),'action':("districtWiseEmployee"),'target':("_self")],2)
printHtmlPart(14)
expressionOut.print(params.graphMap)
printHtmlPart(15)
expressionOut.print(params.graphMap)
printHtmlPart(16)
expressionOut.print(params.graphMapYear)
printHtmlPart(17)
expressionOut.print(params.graphMapYear)
printHtmlPart(16)
expressionOut.print(params.graphMapGender)
printHtmlPart(18)
expressionOut.print(params.graphMapGender)
printHtmlPart(16)
expressionOut.print(params.graphMapDistrict)
printHtmlPart(19)
expressionOut.print(params.graphMapDistrict)
printHtmlPart(20)
})
invokeTag('captureBody','sitemesh',70,[:],1)
printHtmlPart(21)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1440050054000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
