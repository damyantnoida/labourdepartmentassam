import java.text.SimpleDateFormat
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_labourDepartmentAssam_employeeQRGenerateqrUpdate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/employeeQRGenerate/qrUpdate.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',13,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
expressionOut.print(resource(dir: 'css', file: 'jquery-ui.min.css'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js', file: 'myJquery.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'js', file: 'jquery.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'js', file: 'jquery-ui.min.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'js', file: 'validate.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'js', file: 'validation.js'))
printHtmlPart(6)
expressionOut.print(studentInstance?.gender)
printHtmlPart(7)
expressionOut.print(studentInstance?.category)
printHtmlPart(8)
expressionOut.print(studentInstance?.nationality)
printHtmlPart(9)
expressionOut.print(studentInstance?.state)
printHtmlPart(10)
})
invokeTag('captureHead','sitemesh',33,[:],1)
printHtmlPart(11)
createTagBody(1, {->
printHtmlPart(12)

java.text.SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

printHtmlPart(1)
if(true && (flash.message)) {
printHtmlPart(13)
expressionOut.print(flash.message)
printHtmlPart(14)
}
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(15)
invokeTag('hiddenField','g',50,['name':("studentId"),'value':(studentInstance?.id)],-1)
printHtmlPart(16)
expressionOut.print(studentInstance?.firstName)
printHtmlPart(17)
expressionOut.print(studentInstance?.middleName)
printHtmlPart(18)
expressionOut.print(studentInstance?.lastName)
printHtmlPart(19)
if(true && (studentInstance)) {
printHtmlPart(20)
expressionOut.print(studentInstance?.identificationNo)
printHtmlPart(21)
}
printHtmlPart(22)
invokeTag('formatDate','g',87,['format':("dd/MM/yyyy"),'date':(studentInstance?.dob)],-1)
printHtmlPart(23)
invokeTag('select','g',154,['name':("workDistrict"),'id':("workDistrict"),'class':("col-sm-7 form-control"),'value':(studentInstance?.workDistrict),'from':(districtList),'noSelection':(['': ' Select District'])],-1)
printHtmlPart(24)
expressionOut.print(studentInstance?.address)
printHtmlPart(25)
expressionOut.print(studentInstance?.addressCity)
printHtmlPart(26)
expressionOut.print(studentInstance?.addressPO)
printHtmlPart(27)
expressionOut.print(studentInstance?.addressDistrict)
printHtmlPart(28)
expressionOut.print(studentInstance?.addressState)
printHtmlPart(29)
expressionOut.print(studentInstance?.addressPinCode)
printHtmlPart(30)
expressionOut.print(studentInstance?.mobileNo)
printHtmlPart(31)
if(true && (studentInstance)) {
printHtmlPart(32)
if(true && (workerSocialSecurity.size()>0)) {
printHtmlPart(33)
loop:{
int sIndex = 0
for( social in (workerSocialSecurity.socialSecurity) ) {
printHtmlPart(34)
invokeTag('select','g',253,['name':("socialSecurity"),'id':("socialSecurity${sIndex}"),'optionValue':("name"),'optionKey':("id"),'style':("margin-bottom: 3px;"),'class':("col-sm-7 form-control socialSecuritySelect"),'value':(social.id),'from':(socialSecurityList),'noSelection':(['': ' Select Social Security Scheme'])],-1)
printHtmlPart(33)
sIndex++
}
}
printHtmlPart(35)
}
else {
printHtmlPart(33)
invokeTag('select','g',257,['name':("socialSecurity"),'id':("socialSecurity0"),'optionValue':("name"),'optionKey':("id"),'style':("margin-bottom: 3px;"),'class':("col-sm-7 form-control socialSecuritySelect"),'value':(""),'from':(socialSecurityList),'noSelection':(['': ' Select Social Security Scheme'])],-1)
printHtmlPart(35)
}
printHtmlPart(36)
}
printHtmlPart(37)
if(true && (studentInstance)) {
printHtmlPart(38)
expressionOut.print(resource(dir: 'studentImage', file: picture))
printHtmlPart(39)
}
else {
printHtmlPart(40)
}
printHtmlPart(41)
})
invokeTag('uploadForm','g',290,['controller':("employeeQRGenerate"),'action':("submitRegistration"),'method':("post"),'enctype':("multipart/form-data"),'id':("studentRegister"),'name':("studentRegister")],2)
printHtmlPart(42)
})
invokeTag('captureBody','sitemesh',322,[:],1)
printHtmlPart(43)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1439974894000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
