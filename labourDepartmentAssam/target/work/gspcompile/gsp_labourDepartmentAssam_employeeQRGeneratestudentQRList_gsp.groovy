import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_labourDepartmentAssam_employeeQRGeneratestudentQRList_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/employeeQRGenerate/studentQRList.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',10,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
createTagBody(2, {->
invokeTag('captureTitle','sitemesh',11,[:],-1)
})
invokeTag('wrapTitleTag','sitemesh',11,[:],2)
printHtmlPart(3)
expressionOut.print(resource(dir: 'js', file: 'qrcode.min.js'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js', file: 'jquery.js'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js', file: 'myJquery.js'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'js', file: 'jquery.dataTables.min.js'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'css', file: 'dataTable-bootstrap.css'))
printHtmlPart(6)
})
invokeTag('captureHead','sitemesh',17,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(7)
loop:{
int inde = 0
for( student in (stuList) ) {
printHtmlPart(8)
expressionOut.print(student.firstName)
printHtmlPart(9)
expressionOut.print(student.lastName)
printHtmlPart(10)
expressionOut.print(student.identificationNo)
printHtmlPart(11)
expressionOut.print(student.id)
printHtmlPart(12)
createClosureForHtmlPart(13, 3)
invokeTag('link','g',39,['controller':("employeeQRGenerate"),'action':("loadWorkerSocialSecurityDetails"),'id':(student.id),'target':("_blank")],3)
printHtmlPart(14)
inde++
}
}
printHtmlPart(15)
expressionOut.print(resource(dir: 'images', file: 'labour1.jpg'))
printHtmlPart(16)
expressionOut.print(resource(dir: 'images', file: 'signature_scan.gif'))
printHtmlPart(17)
})
invokeTag('captureBody','sitemesh',107,[:],1)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1439962831000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
