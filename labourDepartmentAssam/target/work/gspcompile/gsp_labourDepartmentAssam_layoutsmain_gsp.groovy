import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_labourDepartmentAssam_layoutsmain_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/main.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Content-Type"),'content':("text/html; charset=UTF-8")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("X-UA-Compatible"),'content':("IE=edge,chrome=1")],-1)
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',10,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',10,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',11,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("viewport"),'content':("width=device-width, initial-scale=1.0")],-1)
printHtmlPart(3)
expressionOut.print(resource(dir: 'images', file: 'govtofAssam.png'))
printHtmlPart(4)
expressionOut.print(resource(dir: 'images', file: 'govtofAssam.png'))
printHtmlPart(5)
expressionOut.print(resource(dir: 'images', file: 'govtofAssam.png'))
printHtmlPart(6)
expressionOut.print(resource(dir: 'css', file: 'bootstrap.min.css'))
printHtmlPart(7)
expressionOut.print(resource(dir: 'css/font-awesome-4.3.0/css', file: 'font-awesome.min.css'))
printHtmlPart(7)
expressionOut.print(resource(dir: 'css', file: 'main.css'))
printHtmlPart(8)
expressionOut.print(resource(dir: 'js', file: 'base.js'))
printHtmlPart(9)
expressionOut.print(resource(dir: 'js', file: 'jquery.js'))
printHtmlPart(9)
expressionOut.print(resource(dir: 'js', file: 'main.js'))
printHtmlPart(9)
expressionOut.print(resource(dir: 'js', file: 'bootstrap.min.js'))
printHtmlPart(10)
invokeTag('layoutHead','g',22,[:],-1)
printHtmlPart(1)
invokeTag('javascript','g',23,['library':("application")],-1)
printHtmlPart(1)
invokeTag('layoutResources','r',24,[:],-1)
printHtmlPart(11)
})
invokeTag('captureHead','sitemesh',25,[:],1)
printHtmlPart(12)
createTagBody(1, {->
printHtmlPart(13)
invokeTag('render','g',29,['template':("/layouts/topNavBar")],-1)
printHtmlPart(14)
invokeTag('layoutBody','g',32,[:],-1)
printHtmlPart(15)
invokeTag('layoutResources','r',34,[:],-1)
printHtmlPart(11)
invokeTag('render','g',35,['template':("/layouts/footer")],-1)
printHtmlPart(11)
})
invokeTag('captureBody','sitemesh',36,[:],1)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1439965549000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
