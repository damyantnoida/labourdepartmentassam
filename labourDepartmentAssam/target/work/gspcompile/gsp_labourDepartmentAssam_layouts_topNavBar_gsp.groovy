import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_labourDepartmentAssam_layouts_topNavBar_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/_topNavBar.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('img','g',2,['dir':("images"),'file':("labour1.jpg"),'style':("width: 100%;margin: 5px auto; border-radius: 6px;")],-1)
printHtmlPart(1)
createClosureForHtmlPart(2, 1)
invokeTag('link','g',14,['controller':("home"),'action':("index")],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',21,['controller':("employeeQRGenerate"),'action':("qrUpdate")],2)
printHtmlPart(6)
createClosureForHtmlPart(7, 2)
invokeTag('link','g',24,['controller':("employeeQRGenerate"),'action':("studentQRList")],2)
printHtmlPart(8)
createClosureForHtmlPart(9, 2)
invokeTag('link','g',28,['controller':("employeeQRGenerate"),'action':("report")],2)
printHtmlPart(10)
createClosureForHtmlPart(11, 2)
invokeTag('link','g',30,['controller':("employeeQRGenerate"),'action':("schemeWiseReport")],2)
printHtmlPart(10)
createClosureForHtmlPart(12, 2)
invokeTag('link','g',31,['controller':("employeeQRGenerate"),'action':("districtWiseReport")],2)
printHtmlPart(13)
})
invokeTag('ifLoggedIn','sec',31,[:],1)
printHtmlPart(14)
createTagBody(1, {->
printHtmlPart(15)
createClosureForHtmlPart(16, 2)
invokeTag('ifAnyGranted','sec',38,['roles':("ROLE_ADMIN")],2)
printHtmlPart(4)
createClosureForHtmlPart(17, 2)
invokeTag('link','g',40,['controller':("logout")],2)
printHtmlPart(18)
})
invokeTag('ifLoggedIn','sec',41,[:],1)
printHtmlPart(19)
createTagBody(1, {->
printHtmlPart(20)
createClosureForHtmlPart(21, 2)
invokeTag('link','g',46,['controller':("signUp"),'action':("signUp")],2)
printHtmlPart(6)
createClosureForHtmlPart(22, 2)
invokeTag('link','g',50,['controller':("login"),'action':("auth")],2)
printHtmlPart(18)
})
invokeTag('ifNotLoggedIn','sec',50,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1439988686000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
