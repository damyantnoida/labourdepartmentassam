<%--
  Created by IntelliJ IDEA.
  User: chandan's
  Date: 03-08-2015
  Time: 11:31 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="${resource(dir: 'js/realperson', file: 'jquery.realperson.css')}" type="text/css">
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/realperson', file: 'jquery.plugin.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/realperson', file: 'jquery.realperson.js')}'></script>
    <script>
        $(function() {
            $('#icode').realperson();
        });
    </script>
</head>

<body>
<div class="container">
    <div id="signupbox" style="margin-top:20px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"></div>
            </div>
            <div class="panel-body" >
                <form id="signupform" class="form-horizontal" role="form">

                    <div id="signupalert" style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">First Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-md-3 control-label">Address</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="address" id="address" placeholder="Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-md-3 control-label">City</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="city" id="city" placeholder="City">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="district" class="col-md-3 control-label">District</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="district" id="district" placeholder="District">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="state" class="col-md-3 control-label">State</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="state" id="state" placeholder="State">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pincode" class="col-md-3 control-label">Pincode</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Pincode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phoneNo" class="col-md-3 control-label">Phone Number</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phoneNo" id="phoneNo" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobileNo" class="col-md-3 control-label">Mobile Number</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="mobileNo" id="mobileNo" placeholder="Mobile Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="attachments" class="col-md-3 control-label">Attachments</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control" name="attachments" id="attachments" placeholder="Mobile Number">
                        </div>
                    </div>



                    %{--<div class="form-group">--}%
                        %{--<div class="col-md-3"></div>--}%
                        %{--<div class="col-md-9">--}%
                            %{--<input type="text" class="form-control" name="icode" id="icode">--}%
                        %{--</div>--}%
                    %{--</div> --}%
                    <div class="form-group">



                        <div class="col-md-3"><img src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/></div>
                        <div class="col-md-9">
                            <g:textField name="captcha" class="form-control" placeholder="Type the letters in the box"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                            %{--<span style="margin-left:8px;">or</span>--}%
                        </div>
                    </div>
                    %{--<div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">--}%

                        %{--<div class="col-md-offset-3 col-md-9">--}%
                            %{--<button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>--}%
                        %{--</div>--}%

                    %{--</div>--}%
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>