<head>
<meta name='layout' content='main' />
<title><g:message code="springSecurity.denied.title" /></title>
</head>

<body>
<div class='body'>
    <div class="container">
        <div class="jumbotron">
            <img src="${resource(dir: 'images', file: 'authorized.png')}" style="width: 90%;margin: auto;" class="img-circle"/>
        </div>
    </div>
</div>
</body>
