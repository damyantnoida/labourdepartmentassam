<footer id="footer" class="midnight-blue">

    <div class="container">
        <div id="row">
            <div class="col-sm-6">
                <ul class="pull-left">
                    <li><a href="#about"><i class="fa fa-empire fa-fw"></i>&nbsp;About Us</a></li>
                    <li><a href="#contact"><i class="fa fa-envelope fa-fw"></i>&nbsp;Contact Us</a></li>
                </ul>
                %{--</div>--}%
            </div>

            <div class="col-sm-6">
                <ul class="pull-right">
                    <li style="color: #ffffff">
                        <i class="fa fa-copyright fa-fw"></i> Damyant Software Pvt. Ltd.
                    </li>
                    <li class="twitter-follow">
                        <span class="fa-stack fa-fw">
                            <i class="fa fa-twitter fa-stack-2x"></i>
                        </span>

                    </li><!--//twitter-follow-->
                    <li class="twitter-tweet">
                        <span class="fa-stack fa-fw">
                            <i class="fa fa-facebook fa-stack-2x"></i>
                        </span>

                    </li>
                    <li class="facebook-like">
                        <span class="fa-stack fa-fw">
                            <i class="fa fa-skype fa-stack-2x"></i>
                        </span>
                    </li>
                    <li class="facebook-like">
                        <span class="fa-stack fa-fw">
                            <i class="fa fa-linkedin fa-stack-2x"></i>
                        </span>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    %{--<div class="container text-center">--}%
    %{--<p class="text-muted">.</p>--}%
    %{--</div>--}%
</footer>

