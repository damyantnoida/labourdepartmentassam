
    <!-- Left column -->
    <a href="#"><strong><i class="glyphicon glyphicon-wrench"></i> Tools</strong></a>

    <hr>

    <ul class="list-unstyled">
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">
            <h5>Settings <i class="glyphicon glyphicon-chevron-down"></i></h5>
        </a>
            <ul class="list-unstyled collapse in" id="userMenu">
                <li class="active"> <a href="#"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-envelope"></i> Messages <span class="badge badge-info">4</span></a></li>
                <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Options</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-comment"></i> Shoutbox</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-user"></i> Staff List</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-flag"></i> Transactions</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-exclamation-sign"></i> Rules</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
            </ul>
        </li>
        <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu2">
            <h5>Reports <i class="glyphicon glyphicon-chevron-right"></i></h5>
        </a>

            <ul class="list-unstyled collapse" id="menu2">
                <li><a href="#">Information &amp; Stats</a>
                </li>
                <li><a href="#">Views</a>
                </li>
                <li><a href="#">Requests</a>
                </li>
                <li><a href="#">Timetable</a>
                </li>
                <li><a href="#">Alerts</a>
                </li>
            </ul>
        </li>
        <li class="nav-header">
            <a href="#" data-toggle="collapse" data-target="#menu3">
                <h5>Social Media <i class="glyphicon glyphicon-chevron-right"></i></h5>
            </a>

            <ul class="list-unstyled collapse" id="menu3">
                <li><a href="#"><i class="glyphicon glyphicon-circle"></i> Facebook</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-circle"></i> Twitter</a></li>
            </ul>
        </li>
    </ul>

    <hr>

    <a href="#"><strong><i class="glyphicon glyphicon-link"></i> Resources</strong></a>

    <hr>

    <ul class="nav nav-pills nav-stacked">
        <li class="nav-header"></li>
        <li><a href="#"><i class="glyphicon glyphicon-list"></i> Layouts &amp; Templates</a></li>
        <li><a href="#"><i class="glyphicon glyphicon-briefcase"></i> Toolbox</a></li>
        <li><a href="#"><i class="glyphicon glyphicon-link"></i> Widgets</a></li>
        <li><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Reports</a></li>
        <li><a href="#"><i class="glyphicon glyphicon-book"></i> Pages</a></li>
        <li><a href="#"><i class="glyphicon glyphicon-star"></i> Social Media</a></li>
    </ul>

    <hr>
    <ul class="nav nav-stacked">
        <li class="active"><a href="http://bootply.com" title="The Bootstrap Playground" target="ext">Playground</a></li>
        <li><a href="/tagged/bootstrap-3">Bootstrap 3</a></li>
        <li><a href="/61518" title="Bootstrap 3 Panel">Panels</a></li>
        <li><a href="/61521" title="Bootstrap 3 Icons">Glyphicons</a></li>
        <li><a href="/62603">Layout</a></li>
    </ul>

    <hr>

    <a href="#"><strong>Want More Templates?</strong></a>

    <hr>

    <ul class="nav nav-stacked">
        <li class="active"><a rel="nofollow" href="http://goo.gl/pQoXEh" target="ext">Premium Themes</a></li>
        <li><a rel="nofollow" href="http://gridgum.com/themes/category/bootstrap-themes/?affiliates=45">GridGum</a></li>
        <li><a rel="nofollow" href="http://bootstrapzero.com">BootstrapZero</a></li>
    </ul>
















    %{--<style>--}%
    %{--#ajaxLogin {--}%
        %{--background-color: #EEEEFF;--}%
        %{--display: none;--}%
    %{--}--}%
    %{--#ajaxLogin .inner {--}%
        %{--width: 260px;--}%
        %{--margin:0px auto;--}%
        %{--text-align:left;--}%
        %{--padding:10px;--}%
        %{--border-top:1px dashed #499ede;--}%
        %{--border-bottom:1px dashed #499ede;--}%
        %{--background-color:#EEF;--}%
    %{--}--}%
    %{--#ajaxLogin .inner .fheader {--}%
        %{--padding:4px;margin:3px 0px 3px 0;color:#2e3741;font-size:14px;font-weight:bold;--}%
    %{--}--}%
    %{--#ajaxLogin .inner .cssform p {--}%
        %{--clear: left;--}%
        %{--margin: 0;--}%
        %{--padding: 5px 0 8px 0;--}%
        %{--padding-left: 105px;--}%
        %{--border-top: 1px dashed gray;--}%
        %{--margin-bottom: 10px;--}%
        %{--height: 1%;--}%
    %{--}--}%
    %{--#ajaxLogin .inner .cssform input[type='text'] {--}%
        %{--width: 120px;--}%
    %{--}--}%
    %{--#ajaxLogin .inner .cssform label{--}%
        %{--font-weight: bold;--}%
        %{--float: left;--}%
        %{--margin-left: -105px;--}%
        %{--width: 100px;--}%
    %{--}--}%
    %{--#ajaxLogin .inner .login_message {color:red;}--}%
    %{--#ajaxLogin .inner .text_ {width:120px;}--}%
    %{--#ajaxLogin .inner .chk {height:12px;}--}%
    %{--.errorMessage { color: red; }--}%
    %{--</style>--}%
    %{--<div id='ajaxLogin' class="jqmWindow" style="z-index: 3000;">--}%
        %{--<div class='inner'>--}%
            %{--<div class='fheader'>Please Login..</div>--}%
            %{--<form action='${request.contextPath}/j_spring_security_check' method='POST'--}%
                  %{--id='ajaxLoginForm' name='ajaxLoginForm' class='cssform'>--}%
                %{--<p>--}%
                    %{--<label for='username'>Login ID</label>--}%
                    %{--<input type='text' class='text_' name='j_username' id='username' />--}%
                %{--</p>--}%
                %{--<p>--}%
                    %{--<label for='password'>Password</label>--}%
                    %{--<input type='password' class='text_' name='j_password' id='password' />--}%
                %{--</p>--}%
                %{--<p>--}%
                    %{--<label for='remember_me'>Remember me</label>--}%
                    %{--<input type='checkbox' class='chk' id='remember_me'--}%
                           %{--name='_spring_security_remember_me'/>--}%
                %{--</p>--}%
                %{--<p>--}%
                    %{--<span class="button" id="authAjax">Login</span>--}%
                    %{--<span class="button" id="cancelLogin">Cancel</span>--}%
                %{--</p>--}%
            %{--</form>--}%
            %{--<div style='display: none; text-align: left;' id='loginMessage'></div>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<script type='text/javascript'>--}%
        %{--var onLogin;--}%
        %{--$.ajaxSetup({--}%
            %{--beforeSend : function(xhr, event) {--}%
                %{--// save the 'success' function for later use--}%
                %{--onLogin = event.success;--}%
            %{--},--}%
            %{--statusCode: {--}%
                %{--// Set up a global AJAX error handler to handle the 401--}%
                %{--// unauthorized responses. If a 401 status code comes back,--}%
                %{--// the user is no longer logged-into the system and can not--}%
                %{--// use it properly.--}%
                %{--401: function() {--}%
                    %{--showLogin();--}%
                %{--}--}%
            %{--}--}%
        %{--});--}%

        %{--function showLogin() {--}%
            %{--var ajaxLogin = $('#ajaxLogin');--}%
            %{--ajaxLogin.css('text-align','center');--}%
            %{--// use jqModal to show and align login panel--}%
            %{--ajaxLogin.jqmShow();--}%
        %{--}--}%

        %{--function cancelLogin() {--}%
            %{--$('#ajaxLogin').jqmHide();--}%
        %{--}--}%

        %{--function authAjax() {--}%
            %{--$('#loginMessage').html('Sending request ...').show();--}%

            %{--var form = $('#ajaxLoginForm');--}%
            %{--var config = {--}%
                %{--type : 'post'--}%
                %{--,url : form.attr('action')--}%
                %{--,data : form.serialize()--}%
                %{--,async : false--}%
                %{--,dataType : 'JSON'--}%
                %{--,success: function(response) {--}%
                    %{--form[0].reset();--}%
                    %{--$('#loginMessage').empty();--}%
                    %{--$('#ajaxLogin').jqmHide();--}%
                    %{--if (onLogin) {--}%
                        %{--// execute the saved event.success function--}%
                        %{--onLogin(response);--}%
                    %{--}--}%
                %{--}--}%
                %{--,error : function (response) {--}%
                    %{--var responseText = response.responseText || '[]';--}%
                    %{--var json = responseText.evalJSON();--}%
                    %{--if (json.error) {--}%
                        %{--$('#loginMessage').html("<span class='errorMessage'>" + json.error + '</error>');--}%
                    %{--} else {--}%
                        %{--$('#loginMessage').html(responseText);--}%
                    %{--}--}%
                %{--}--}%
                %{--,beforeSend : function(xhr, event) {--}%
                    %{--//console.log("overriding default behaviour");--}%
                %{--}--}%
            %{--}--}%
            %{--$.ajax(config);--}%
        %{--}--}%

        %{--$(function() {--}%
            %{--$('#ajaxLogin').jqm({modal: true, trigger: 'span.jqmTrigger'});--}%
            %{--$('#authAjax').click(authAjax);--}%
            %{--$('#cancelLogin').click(cancelLogin);--}%
        %{--});--}%
    %{--</script>--}%