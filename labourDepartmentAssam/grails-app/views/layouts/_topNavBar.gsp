<div class="container topImageHeader">
    <g:img dir="images" file="labour1.jpg" style="width: 100%;margin: 5px auto; border-radius: 6px;"/>
</div>

<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        %{--<a class="navbar-brand" href="#" style=" padding: 1px;margin-right: 10px;vertical-align: bottom;"><img src="${resource(dir: 'images', file: 'symphonie11.png')}" alt="insuranceApp" style="height: 70%; margin-top: 7%;"/></a>--}%
    </div>

    <div id="navbar" class="navbar-collapse collapse navbar-other">

        <ul class="nav navbar-nav">

            <li><g:link controller="home" action="index"><i class="fa fa-home fa-fw"></i>Home</g:link></li>
            <sec:ifLoggedIn>
                <li><g:link controller="employeeQRGenerate" action="qrUpdate"><i
                        class="glyphicon glyphicon-user fa-fw"></i>Worker Registration</g:link></li>
                <li><g:link controller="employeeQRGenerate" action="studentQRList"><i
                        class="glyphicon glyphicon-user fa-fw"></i>Worker List</g:link></li>
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i
                        class="glyphicon glyphicon-stats fa-fw"></i>Analytics<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><g:link controller="employeeQRGenerate" action="report">Graphical Analytics</g:link></li>
                    <li><g:link controller="employeeQRGenerate" action="schemeWiseReport">Scheme Wise Financial Details</g:link></li>
                    <li><g:link controller="employeeQRGenerate" action="districtWiseReport">District Wise Financial Details</g:link></li>
                </ul></li>
            </sec:ifLoggedIn>
        </ul>

        <sec:ifLoggedIn>
            <ul class="nav navbar-nav navbar-right">
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li><a href="#"><i class="fa fa-user-md fa-fw"></i> Manage</a>
                    </li>
                </sec:ifAnyGranted>
                <li><g:link controller="logout">
                    <i class="fa fa-sign-out fa-fw"></i>
                    </span>Logout</g:link></li>
            </ul>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <ul class="nav navbar-nav navbar-right">
                <li><g:link controller="signUp" action="signUp"><i class="fa fa-pencil fa-fw"></i>Sign up</g:link></li>
                <li><g:link controller="login" action="auth">
                    <i class="fa fa-sign-in fa-fw"></i>

                    Sign in
                </g:link></li>
            </ul>
        </sec:ifNotLoggedIn>
    </div>
</div>