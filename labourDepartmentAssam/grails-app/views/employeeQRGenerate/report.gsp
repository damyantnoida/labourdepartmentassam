<%--
  Created by IntelliJ IDEA.
  User: chandan's
  Date: 07-08-2015
  Time: 10:20 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'myJquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/Highcharts-4.1.7/js', file: 'highcharts.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/Highcharts-4.1.7/js', file: 'highcharts-3d.js')}'></script>

    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/Highcharts-4.1.7/js/modules', file: 'exporting.js')}'></script>
    %{--<script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/Highcharts-4.1.7/js/modules', file: 'canvas-tools.js')}'></script>--}%
    %{--<script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/Highcharts-4.1.7/js/modules', file: 'broken-axis.js')}'></script>--}%
    <script type="text/javascript">
        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("?"));
            window.history.replaceState({}, document.title, clean_uri);
        }
    </script>
</head>

<body>
<h1 class="text-center page-header">Reports</h1>
<ul class="list-group">
    <li class="list-group-item"><g:link controller="employeeQRGenerate" action="ageGroupGraph" target="_self">Different Age Groups</g:link> </li>
    <li class="list-group-item"><g:link controller="employeeQRGenerate" action="yearWiseRegistration" target="_self">Year Wise Total Registration</g:link></li>
    <li class="list-group-item"><g:link controller="employeeQRGenerate" action="genderComparison" target="_self">Gender Comparison</g:link></li>
    <li class="list-group-item"><g:link controller="employeeQRGenerate" action="districtWiseEmployee" target="_self">District wise comparison of workers</g:link></li>
</ul>
<span class="col-sm-4 text-center" style="display: none">
    <button type="button" id="Generate" class="btn btn-default" data-toggle="modal"
            data-target="#myModal">
        Generate
    </button>
</span>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"  style="height: 500px;width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Report</h4>
            </div>
            <div class="modal-body"  id="graph" style="height: 450px;width: 780px;margin: auto;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        if ("${params.graphMap}") {
            $('#myModal').modal('show')
            drawBarChart("${params.graphMap}");

        }
        else if("${params.graphMapYear}") {
            $('#myModal').modal('show')
            drawYearBarChart("${params.graphMapYear}");

        }
        else if("${params.graphMapGender}") {
            $('#myModal').modal('show')
            drawGenderPieChart("${params.graphMapGender}");

        }
        else if("${params.graphMapDistrict}") {
            $('#myModal').modal('show')
            drawDistrictPieChart("${params.graphMapDistrict}");

        }

    });
</script>
</body>
</html>