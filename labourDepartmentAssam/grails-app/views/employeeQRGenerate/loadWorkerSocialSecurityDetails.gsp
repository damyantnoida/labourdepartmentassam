<%--
  Created by IntelliJ IDEA.
  User: chandan's
  Date: 18-08-2015
  Time: 02:36 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title></title>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'myJquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.bpopup.min.js')}'></script>
</head>
<body>
<div class="marketing">
    <h1 class="text-center page-header">Worker Information</h1>
    <div class="col-sm-12 form-group">
        <div class="col-sm-8">
            <div class="col-sm-12 form-inline form-group">
                <div class="col-sm-5">Name of the applicant</div>
                <div class="col-sm-7">${studentInstance?.firstName} ${studentInstance?.middleName}  ${studentInstance?.lastName}
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Social Security Schemes</div>

                <div class="col-sm-7">
                    <g:if test="${socialSecList.size()>0}">
                        <g:each in="${socialSecList}" var="scheme">
                            <div style="margin: 5px 0px;font-weight: bold;"><a href="#" onclick="showSocialSecurityDetails(${scheme.socialSecurity.id})">${scheme.socialSecurity.name}</a> </div>
                        </g:each>
                    </g:if>
                    <g:else>
                        Not Assigned Any Scheme
                    </g:else>

                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Identification No</div>
                <div class="col-sm-3">${studentInstance?.identificationNo}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Date of Birth</div>

                <div class="col-sm-3"><g:formatDate format="dd/MMM/yyyy" date="${studentInstance?.dob}"/>
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Category</div>

                <div class="col-sm-5">${studentInstance?.category}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Nationality</div>

                <div class="col-sm-5">${studentInstance?.nationality}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Gender</div>

                <div class="col-sm-5 ">${studentInstance.gender}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">State of Domicile</div>

                <div class="col-sm-5">${studentInstance.state}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Working District</div>

                <div class="col-sm-5"><strong>${studentInstance.workDistrict}</strong>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Address</div>

                <div class="col-sm-5">${studentInstance?.address}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Village/Town</div>

                <div class="col-sm-5">${studentInstance?.addressCity}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Post Office</div>

                <div class="col-sm-5">${studentInstance?.addressPO}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">District</div>

                <div class="col-sm-5">${studentInstance?.addressDistrict}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">State</div>

                <div class="col-sm-5">${studentInstance?.addressState}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Pincode</div>

                <div class="col-sm-5">${studentInstance?.addressPinCode}
                </div>
            </div>

            <div class="col-sm-12 form-group">
                <div class="col-sm-5">Contact Number</div>

                <div class="col-sm-5">${studentInstance?.mobileNo}
                </div>
            </div>


        </div>

        <div class="col-sm-4" style="vertical-align: top;">
            <g:if test="${studentInstance}">
                <img src="${resource(dir: 'studentImage', file: picture)}" class="img-thumbnail" width="180" id="picture1"/>
            </g:if>
        </div>
    </div>


</div>
<div style="display: none;">
    <div id="popup" style="position: absolute; opacity: 1; display: block;font-weight: bold;">
        <span class="button b-close"><span>X</span></span>
        <div  class="col-sm-12 text-center page-header" style="margin-top:-20px;padding: 4px ;"><h2 id="schemeHeader"></h2></div>
        <div  class="col-sm-12" style="margin: auto;padding: 4px 0px;">
            <span  class="col-sm-9">Max Benefit Entitlement Per Month (&#8377):</span><span id="benefitAmount"  class="col-sm-3"></span>
        </div>
        <div class="col-sm-12"  style="margin: auto;padding: 4px 0px;">
            <span   class="col-sm-9">Total Amount transferred in FY 2014 (&#8377):</span><span  id="trnansferredAmount" class="col-sm-3"></span>
        </div>
        <div class="col-sm-12"  style="margin: auto;padding: 4px 0px;">
            <span class="col-sm-5">Bank Details:</span>
            <span class="col-sm-7">
                <span class="col-sm-12">State Bank Of India</span>
                <span class="col-sm-12">A/C No. 000045232222</span>
                <span class="col-sm-12">Branch: Guwahati</span>
                <span class="col-sm-12">IFSC Code: SBIN000078</span>
            </span>
        </div>
    </div>
</div>
</body>

</html>