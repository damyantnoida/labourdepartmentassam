<%--
  Created by IntelliJ IDEA.
  User: chandan's
  Date: 03-08-2015
  Time: 05:08 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title></title>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'qrcode.min.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'myJquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.dataTables.min.js')}'></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'dataTable-bootstrap.css')}" type="text/css">
</head>

<body>
<div class="marketing">
    <table class="table table-hover" id="userListTable">
        <thead>
        <tr>
            <th class="col-sm-3 text-center"><strong>Name</strong></th>
            <th class="col-sm-3 text-center"><strong>Identification No</strong></th>
            <th class="col-sm-3 text-center"><strong>Click here to Generate ID</strong></th>
            <th class="col-sm-3 text-center"><strong>View Details</strong></th>
            %{--<th class="col-sm-4 text-center"><strong>Social Security Sc</strong></th>--}%
        </tr>
        </thead>
        <tbody>
        <g:each in="${stuList}" var="student" status="inde">
            <tr>
                <td class="col-sm-3 text-center">${student.firstName} ${student.lastName}</td>
                <td class="col-sm-3 text-center">${student.identificationNo}</td>
                <td class="col-sm-3 text-center"><input type="button" value="Generate" class="btn btn-default"
                                                        onclick="generateIdentity(${student.id})"/></td>
                <td class="col-sm-3 text-center"><g:link controller="employeeQRGenerate" action="loadWorkerSocialSecurityDetails" id="${student.id}" target="_blank" ><button class="btn btn-default"> View </button></g:link> </td>
                <span class="col-sm-4 text-center" style="display: none">
                    <button type="button" id="Generate" class="btn btn-default" data-toggle="modal"
                            data-target="#myModal">Generate</button>
                </span>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Identity Card</h4>
            </div>

            <div class="modal-body" style="height: 350px;">
                <div style="width: 100%;margin-bottom: 8px;" id="idHeader">
                    <img src="${resource(dir: 'images', file: 'labour1.jpg')}" style="width: 100%;">
                </div>

                <div style="width: 100%;">
                    <div id="idDetails" style="width: 60%;line-height: 20px;float: left;">
                        <div style="width: 100%;clear: both;">
                            <label style="width: 40%; text-align: left;float: left;">Name:</label><div style="width: 60%;float: left;" id="nameContent"></div>
                        </div>
                        <div style="width: 100%;clear: both;">
                            <label style="width: 40%; text-align: left;float: left;">Address:</label><div style="width: 60%;float: left;"
                                                                           id="addressContent"></div>
                        </div>
                        <div style="width: 100%;clear: both;">
                            <label style="width: 40%; text-align: left;float: left;">Contact No:</label><div style="width: 60%;float: left;"
                                                                             id="contectContent"></div>
                        </div>
                         <div style="width: 100%;clear: both;">
                            <label style="width: 40%; text-align: left;float: left;">Aadhar No:</label><div style="width: 60%;float: left;"
                                                                             id="aadharContent"></div>
                        </div>

                        <div style="width: 100%;clear: both;">
                            <label style="width: 40%; text-align: left;float: left;">Date of Birth:</label><div style="width: 60%;float: left;"
                                                                                 id="dobContent"></div>
                        </div>
                        <div style="width: 100%;clear: both;">
                            <div id="signature" style="height: 60px;width: 150px;text-align: center;">
                                <img src="${resource(dir: 'images', file: 'signature_scan.gif')}" style="width: 60%;">
                            </div>
                            <label style="width: 100%;">Authorized Signature</label>
                        </div>
                    </div>

                    <div style="width: 40%;float: left;text-align: center;vertical-align: middle;">
                        <img id="picture" style="height:120px;width: 110px;padding: 5px;text-align: center;margin: auto;">
                        </img>
                        <div id="qrCode" style="height:100px;width: 100px;padding: 5px;text-align: center;margin: auto;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#userListTable').DataTable();
    });
</script>
</body>
</html>