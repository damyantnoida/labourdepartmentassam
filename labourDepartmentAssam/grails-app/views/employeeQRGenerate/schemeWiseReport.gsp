<%--
  Created by IntelliJ IDEA.
  User: kamesh
  Date: 19/8/15
  Time: 5:25 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name='layout' content='main'/>
    <title></title>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'myJquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.bpopup.min.js')}'></script>
</head>

<body>
<div class="marketing">
    <h1 class="text-center page-header">Scheme Wise Report</h1>
    <div class="col-lg-12 row bottom-buffer">
        <div class="col-lg-6 text-center h"><strong>Social Security Scheme</strong></div>
        <div class="col-lg-6 text-center"><strong>Total Amount Paid</strong></div>
    </div>
    <g:each in="${schemeNameList}" var="scheme" status="index">
        <div class="tableheadingcolor col-lg-12 row bottom-buffer">
            <div class="col-lg-6 text-center">${scheme.name}</div>
            <div class="col-lg-6 text-center"><a href="#" onclick="loadDrillDownDetails(${scheme.id})"> &#8377; ${amountList[index]}</a></div>
        </div>
    </g:each>
    <div class=" col-lg-12 row bottom-buffer page-header" style="margin-top: 0px;"></div>
    <div class=" col-lg-12 row bottom-buffer">
        <div class="col-lg-6 text-center">Total</div>
        <div class="col-lg-6 text-center">&#8377; ${total}</div>
    </div>
</div>
<div style="display: none;">
    <div id="popup" style="position: absolute; opacity: 1; display: block;font-weight: bold;">
        <span class="button b-close"><span>X</span></span>
        <div  class="col-sm-12 text-center page-header" style="margin-top:-20px;padding: 4px ;"><h2 id="schemeHeader"></h2></div>
        <div id="mainPopupContent"  class="col-sm-12" style="margin: auto;padding: 4px 0px;">
        </div>
    </div>
</div>
</body>
</html>