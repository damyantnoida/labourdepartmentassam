<%@ page import="java.text.SimpleDateFormat" contentType="text/html;charset=UTF-8" %>
<%--
  Created by IntelliJ IDEA.
  User: Kuldeep
  Date: 2/6/14
  Time: 3:36 PM
  To change this template use File | Settings | File Templates.
--%>


<html>
<head>
    <title>Registration</title>
    <meta name="layout" content="main"/>
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.theme.min.css')}" type="text/css">--}%
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery-ui.min.css')}" type="text/css">
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.ui.datepicker.css')}" type="text/css">--}%
    %{--<script type='text/javascript' charset='utf-8' src='${resource(dir: 'js/jquery', file: 'jquery.ui.datepicker.js')}'></script>--}%
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'myJquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'jquery-ui.min.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'validate.js')}'></script>
    <script type='text/javascript' charset='utf-8' src='${resource(dir: 'js', file: 'validation.js')}'></script>
    <script type="text/javascript">
        var gender = "${studentInstance?.gender}"
        var category = "${studentInstance?.category}"
        var nationality = "${studentInstance?.nationality}"
        var state = "${studentInstance?.state}"
        $('#studentRegister').ready(function () {
            if(nationality!='')
            $("input[name='nationality'][value=" + nationality + "]").attr('checked', 'checked');
            if(category!='')
            $("input[name='category'][value='" + category + "']").attr('checked', 'checked');
            if(gender!='')
            $("input[name='gender'][value=" + gender + "]").attr('checked', 'checked');
            if(state!='')
            $("input[name='state'][value=" + state + "]").attr('checked', 'checked');

            $("#submitButton").click(function (event) {
                if ($('#studentRegister').valid()) {
                    $(event.target).attr("hidden", "true");
                }
            });
        });
    </script>
</head>

<body>
<div id="main">
    <% java.text.SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); %>
    <g:if test="${flash.message}">
        <div class="message"><div class="university-status-message">${flash.message}</div></div>
    </g:if>
    <g:uploadForm controller="employeeQRGenerate" action="submitRegistration" method='post'
                  enctype="multipart/form-data"
                  id="studentRegister" name="studentRegister">
        <h1 class="text-center page-header">INFORMATION SHEET</h1>
        <g:hiddenField name="studentId" value="${studentInstance?.id}"/>
        <div class="col-sm-12 form-inline form-group">
            <div class="col-sm-4">Name of the applicant</div>
            <div class="col-sm-8">
                <div class="col-sm-4">
                    <input type="text" placeholder="First Name" name="firstName" style="margin-left: -11px;"
                           onkeypress="return onlyAlphabets(event);"
                           onBlur="this.value = this.value.toUpperCase()"
                           maxlength="50" class="col-sm-4  form-control" value="${studentInstance?.firstName}"/>
                </div>
                <div class="col-sm-4">
                    <input type="text" placeholder="Middle Name" name="middleName" style="margin-left: -11px;"
                           onkeypress="return onlyAlphabets(event);"
                           onBlur="this.value = this.value.toUpperCase()"
                           maxlength="50" class="col-sm-4 form-control" value="${studentInstance?.middleName}"/>
                </div>
                <div class="col-sm-4">
                    <input type="text" placeholder="Last Name" name="lastName" style="margin-left: -11px;"
                           onkeypress="return onlyAlphabets(event);"
                           onBlur="this.value = this.value.toUpperCase()"
                           maxlength="50" class="col-sm-4 form-control " value="${studentInstance?.lastName}"/>
                </div>
            </div>
        </div>
        <g:if test="${studentInstance}">
            <div class="col-sm-12 form-group">
                <div class="col-sm-4">Identification No</div>
                <div class="col-sm-3">
                    <input type="text" name="rollNo" readonly
                           class="col-sm-7  form-control" value="${studentInstance?.identificationNo}"/>
                </div>
            </div>
        </g:if>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Date of Birth</div>

            <div class="col-sm-3">
                <input type="text" id="datepickers" name="d_o_b" maxlength="10" PLACEHOLDER="DD/MM/YYYY" class="col-sm-7  form-control"
                  value="<g:formatDate format="dd/MM/yyyy" date="${studentInstance?.dob}"/>">
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Category</div>

            <div class="col-sm-8 checkbox-inline radio_options">
                <label class="col-sm-2"><input type="radio" name="category" value="General"/><label
                        style="margin-left: 3px;">General</label>
                </label>

                <label class="col-sm-2"><input type="radio" name="category" value="MOBC"/><span
                        style="margin-left: 3px;">MOBC</span></label>

                <label class="col-sm-2"><input type="radio" name="category" value="OBC"/><span
                        style="margin-left: 3px;">OBC</span></label>

                <label class="col-sm-2"><input type="radio" name="category" value="SC"/><span
                        style="margin-left: 3px;">SC</span>
                </label>

                <label class="col-sm-2"><input type="radio" name="category" value="S.T"/><span
                        style="margin-left: 3px;">ST</span></label>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Nationality</div>

            <div class="col-sm-8 checkbox-inline radio_options">
                <label class="col-sm-3"><input type="radio" name="nationality" value="Indian" class="radioInput"/><span
                        style="margin-left: 3px;">Indian</span>
                </label>
                <label class="col-sm-3"><input type="radio" name="nationality" value="Non-Indian"
                                               class="radioInput"/><span style="margin-left: 3px;">Non-Indian</span>
                </label>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Gender</div>

            <div class="col-sm-8 checkbox-inline radio_options">
                <label class="col-sm-3"><input type="radio" name="gender" value="Male" class="radioInput"/><span
                        style="margin-left: 3px;">Male</span></label>
                <label class="col-sm-3"><input type="radio" name="gender" value="Female" class="radioInput"/><span
                        style="margin-left: 3px;">Female</span></label>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">State of Domicile</div>

            <div class="col-sm-8 checkbox-inline radio_options">
                <label class="col-sm-3"><input type="radio" name="state" value="Assam" class="radioInput"/><span
                        style="margin-left: 3px;">Assam</span></label>
                <label class="col-sm-3"><input type="radio" name="state" value="Others" class="radioInput"/><span
                        style="margin-left: 3px;">Others</span></label>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Working District</div>

            <div class="col-sm-5">
                <g:select name="workDistrict" id="workDistrict"
                          class="col-sm-7 form-control" value="${studentInstance?.workDistrict}"
                          from="${districtList}" noSelection="['': ' Select District']"/>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Address</div>

            <div class="col-sm-5">
                <input type="text" id="studentAddress" name="studentAddress" maxlength="30"
                       class="col-sm-7  form-control"
                       value="${studentInstance?.address}"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Village/Town</div>

            <div class="col-sm-5">
                <input type="text" id="townName" name="addressTown" maxlength="30"
                       value="${studentInstance?.addressCity}"
                       onkeypress="return isAlphaNumeric(event);"
                       class="col-sm-7  form-control"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Post Office</div>

            <div class="col-sm-5">
                <input type="text" id="poAddress" name="addressPO"
                       onkeypress="return onlyAlphabets(event);"
                       value="${studentInstance?.addressPO}"
                       maxlength="30" class="col-sm-7  form-control"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">District</div>

            <div class="col-sm-5">
                <input type="text" value="${studentInstance?.addressDistrict}"
                       name="addressDistrict" onkeypress="return onlyAlphabets(event);"
                       maxlength="30" id="addDistrict"
                       class="col-sm-7  form-control"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">State</div>

            <div class="col-sm-5">
                <input type="text" value="${studentInstance?.addressState}"
                       name="addressState"
                       onkeypress="return onlyAlphabets(event);"
                       maxlength="30" id="stateAddress"
                       class="col-sm-7  form-control"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Pincode</div>

            <div class="col-sm-5">
                <input type="text" value="${studentInstance?.addressPinCode}"
                       name="addressPinCode"
                       id="pincode"
                       maxlength="6"
                       class="col-sm-7  form-control"
                       onkeypress="return isNumber(event)"/>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Contact Number</div>

            <div class="col-sm-5">
                <input type="text" value="${studentInstance?.mobileNo}"
                       name="mobileNo"
                       id="mobileNo"
                       maxlength="10"
                       class="col-sm-7  form-control"
                       onkeypress="return isNumber(event)"/>
            </div>
        </div>
        <g:if test="${studentInstance}">
            <div class="col-sm-12 form-group">
                <div class="col-sm-4">Assign Social Security Scheme</div>

                <div class="col-sm-5" id="socialSecurityDiv">
                    <g:if test="${workerSocialSecurity.size()>0}">
                        <g:each in="${workerSocialSecurity.socialSecurity}" var="social" status="sIndex">
                            <g:select name="socialSecurity" id="socialSecurity${sIndex}" optionValue="name" optionKey="id" style="margin-bottom: 3px;"
                                      class="col-sm-7 form-control socialSecuritySelect" value="${social.id}"
                                      from="${socialSecurityList}" noSelection="['': ' Select Social Security Scheme']"/>
                        </g:each>
                    </g:if>
                    <g:else>
                        <g:select name="socialSecurity" id="socialSecurity0" optionValue="name" optionKey="id" style="margin-bottom: 3px;"
                                                     class="col-sm-7 form-control socialSecuritySelect" value=""
                                                     from="${socialSecurityList}" noSelection="['': ' Select Social Security Scheme']"/>
                    </g:else>


                </div>
                <div class="col-sm-3" style="vertical-align: top;">
                    <input type="button" value="Add More" onclick="addMoreSocial()" class="btn btn-info"/>
                    <input type="button" value="Remove" onclick="removeSocial()" class="btn btn-danger"/>
                </div>
            </div>
        </g:if>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4">Passport size Photograph (Size: Less then 50KB )</div>

            <div class="col-sm-5">
                <g:if test="${studentInstance}">
                    <img src="${resource(dir: 'studentImage', file: picture)}" class="img-thumbnail" width="100"
                         height="130"
                         id="picture1"/>
                %{--<input type='file' id="profileImage" onchange="readURL(this, 'picture1');" class="university-button"--}%
                %{--name="photograph"/>--}%
                </g:if>
                <g:else>
                    <div id="profile-image"><img src="" alt="Space for Photograph " class="img-thumbnail" width="100"
                                                 height="130" id="picture2"/></div>
                    <input type='file' id="profileImage" onchange="readURL(this, 'picture2');" class="university-button"
                           name="photograph"/>
                    <input type="text" id="imageValidate" name="imageValidate" value=""
                           style="width: 1px;height: 1px;border: 0px;"/>
                </g:else>

            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class="col-sm-4"></div>

            <div class="col-sm-5">
                <input type="submit" value="Submit" id="submitButton" onclick="validate()" class="btn btn-primary">
                <input type="reset" value="Reset" onclick="resetImage()" class="btn btn-reset">
            </div>
        </div>

    </g:uploadForm>
</div>
<script type="text/javascript">
    $( document ).ready(function() {

        $('#datepickers').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            maxDate: 0
        });
    })
</script>
<script>
    $('#signatureFile').bind('change', function () {
//    alert('This file size is: ' + this.files[0].size/1024/1024 + "MB");
    })
    function resetImage() {
        $("#signature").attr('src', '#')
        $("#picture").attr('src', '#')
    }
    $('#submitButton').on('click', function () {
        if ($('#studentRegister').valid()) {
            setTimeout(function () {
                $('#studentRegister')[0].reset();
                var abc = $('#picture2').remove();
                $('#profile-image').append('<img src="" alt="Space for Photograph " class="university-registration-photo" id="picture2"/>')
//                  location.reload()
            }, 1000)

        }
    })
</script>
</body>
</html>