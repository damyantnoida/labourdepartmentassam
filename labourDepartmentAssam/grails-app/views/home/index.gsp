<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Application</title>
	</head>
	<body>
		<div id="page-body" role="main" class="col-sm-12">
			<h1>Welcome to Application</h1>
			<p>Congratulations, you have successfully started ! At the moment
			   this is the default page, feel free to modify it to either redirect to a controller or display whatever
			   content you may choose.</p>
		</div>
	</body>
</html>
