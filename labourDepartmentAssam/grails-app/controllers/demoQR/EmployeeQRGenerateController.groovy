package demoQR

import fordemo.SocialSecurity
import fordemo.Worker
import fordemo.WorkerSocialSecurity
import grails.converters.JSON
import org.codehaus.groovy.grails.validation.Validateable


import java.text.DateFormat
import java.text.SimpleDateFormat

class EmployeeQRGenerateController {
    def registrationService
    def springSecurityService

    def studentQRList() {
        def stuList = Worker.list()
        [stuList: stuList]
    }

    def qrUpdate() {
        def districtList = ["Karimganj", "Darrang", "Dibrugarh", "Morigaon", "Tinsukia", "Bongaigaon", "Nalbari", "Kokrajhar", "Kamrup", "Karbi-Anglong", "Nagaon", "N.C.Hills", "Dhemaji", "Hailakandi", "Lakhimpur", "Sonitpur", "Dhubri", "Goalpara", "Barpeta", "Golaghat", "Jorhat", "Sibsagar", "Cachar"]

        if (params.studentId) {
            if (springSecurityService.currentUser) {
                def studentInstance = Worker.findById(Long.parseLong(params.studentId))
                def picture = studentInstance.folderYear + "/" + studentInstance.folderNumber + "/" + studentInstance.workerImage
                def socialSecurityList = SocialSecurity.list()
                def workerSocialSecurity = WorkerSocialSecurity.findAllByWorker(studentInstance)
                [workerSocialSecurity: workerSocialSecurity, studentInstance: studentInstance, picture: picture, districtList: districtList, socialSecurityList: socialSecurityList]
            } else {
                redirect(controller: "login", action: "auth")
            }
        } else {
            [districtList: districtList]
        }
    }

    def loadStudentInfo() {
        def resultMap = [:]
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy")
        def studentInstance = Worker.findById(Long.parseLong(params.studentId))
        def date = df.format(studentInstance.dob)
        def regYear = studentInstance.folderYear
        def programCode = studentInstance.folderNumber
        def imageName = studentInstance.workerImage
        def ip = InetAddress.getLocalHost();
        def ipAddress = ip.getHostAddress()
//        def picture = studentInstance.registrationYear + "/" + studentInstance.programDetail[0].courseCode + "/" + studentInstance.imageFileName
//        def webRootDir = servletContext.getRealPath("/")
        resultMap.date = date
        resultMap.regYear = regYear
        resultMap.imageName = imageName
        resultMap.programCode = programCode
        resultMap.studentInstance = studentInstance
        render resultMap as JSON
    }
    def viewImage = {
        def photo = Student.get(params.id)
        byte[] image = photo.studentImage
        response.outputStream << image
    }
    def submitRegistration = {
        def photographe = request.getFile("photograph")
        def studentRegistration = registrationService.saveNewRegistration(params, photographe)
        if (studentRegistration) {
            if (params.studentId) {
                flash.message = "${message(code: 'register.updated.message')}"
                redirect(action: "qrUpdate")

            } else {
                flash.message = "${message(code: 'register.created.message')}" + " And Identification number is :" + studentRegistration.identificationNo
                redirect(action: "qrUpdate")

            }
        } else {
            flash.message = "${message(code: 'register.notCreated.message')}"
            redirect(action: "qrUpdate")
        }
    }
    def report = {

    }
//    def scriptDB = {
//        def districtList = ["Karimganj", "Darrang", "Dibrugarh", "Morigaon", "Tinsukia", "Bongaigaon", "Nalbari", "Kokrajhar", "Kamrup", "Karbi-Anglong", "Nagaon", "N.C.Hills", "Dhemaji", "Hailakandi", "Lakhimpur", "Sonitpur", "Dhubri", "Goalpara", "Barpeta", "Golaghat", "Jorhat", "Sibsagar", "Cachar"]
//        def regYear = ["2005","2006","2007","2008","2009","2010","2011", "2012", "2013", "2014", "2015"]
//        def studentList = Student.findAllByStatusAndRegistrationYearAndRollNoIsNotNullAndImageFileNameIsNotNullAndAddressPOIsNotNullAndStudentAddressIsNotNull(Status.findById(4),2014, [max: 1000, sort: "rollNo", order: "desc"])
//        Random picker = new Random();
//        def count = 0
//        studentList.each {
//            def status = true
//            def workerInst = new Worker()
//            if (it.studentAddress != null)
//                workerInst.address = it.studentAddress
//            else {
//                status = false
//            }
//            workerInst.addressCity = it.addressTown
//            workerInst.addressDistrict = it.addressDistrict
//            workerInst.addressPinCode = it.addressPinCode
//            workerInst.addressState = it.addressState
//            workerInst.state = it.state
//            workerInst.folderYear = it.registrationYear
//            workerInst.folderNumber = it.programDetail[0].courseCode
//            workerInst.category = it.category
//            workerInst.nationality = it.nationality
//            workerInst.nationality = it.nationality
//            workerInst.addressPO = it.addressPO
//            workerInst.mobileNo = it.mobileNo
//            workerInst.dob = it.dob
//            workerInst.firstName = it.firstName
//            workerInst.lastName = it.lastName
//            workerInst.middleName = it.middleName
//            workerInst.gender = it.gender
//            if (it.rollNo != null) {
//                workerInst.identificationNo = it.rollNo
//            } else if (it.referenceNumber != null) {
//                workerInst.identificationNo = it.referenceNumber
//            } else {
//                status = false
//            }
//            workerInst.registrationYear = regYear[picker.nextInt(regYear.size())]
//            workerInst.workDistrict = districtList[picker.nextInt(districtList.size())]
//            workerInst.workerImage = it.imageFileName
//            if (status) {
//                count++
//                workerInst.save(flush: true, failOnError: true)
//            }
//
//        }
//        render("Total " + count + " record uploaded.")
//    }
//    def changeMinority(){
//        def worList=Worker.findAllByCategory("MINORITY COMMUNITY")
//        worList.each{
//            it.category="General"
//        }
//        render "Done"
//    }
    def ageGroupGraph = {
        def graphMap = [:], drillDownMap = [:]
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def ageGroupList = ["0-10", "11-20", "21-30", "31-40", "41-50", "51-60", "61-70"]
        def age10 = 0, age20 = 0, age30 = 0, age40 = 0, age50 = 0, age60 = 0, age70 = 0
        def age10List = [], age20List = [], age30List = [], age40List = [], age50List = [], age60List = [], age70List = []
        def workerList = Worker.list()
        workerList.each {
            def result = ageCalculate(it.dob)
            if ((result >= 0) && (result <= 10)) {
                age10 += 1
            } else if ((result >= 11) && (result <= 20)) {
                age20 += 1
            } else if ((result >= 21) && (result <= 30)) {
                age30 += 1
            } else if ((result >= 31) && (result <= 40)) {
                age40 += 1
            } else if ((result >= 41) && (result <= 50)) {
                age50 += 1
            } else if ((result >= 51) && (result <= 60)) {
                age60 += 1
            } else if ((result >= 61) && (result <= 70)) {
                age70 += 1
            }

        }
        def ageCountList = [age10, age20, age30, age40, age50, age60, age70]
        graphMap.put(0, ageGroupList)
        graphMap.put(1, ageCountList)
        redirect(controller: "employeeQRGenerate", action: "report", params: [graphMap: graphMap as JSON])

    }
    def yearWiseRegistration = {
        def graphMapYear = [:]
        def regCountList = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def yearList = Worker.createCriteria().list {
            projections {
                distinct("registrationYear")
            }
            order("registrationYear", "desc")
        }
        yearList.each {
            def counts = Worker.countByRegistrationYear(it)
            regCountList << counts
        }
        graphMapYear.put(0, yearList)
        graphMapYear.put(1, regCountList)
        redirect(controller: "employeeQRGenerate", action: "report", params: [graphMapYear: graphMapYear as JSON])

    }

    def genderComparison() {
        def graphMapGender = [:]
        def graphLabel = ['Male', 'Female']
        def genderCount = []
        graphLabel.each {
            def typeList = []
            def empCount = Worker.countByGender(it)
            typeList << it
            typeList << empCount
            genderCount << typeList
        }
        redirect(controller: "employeeQRGenerate", action: "report", params: [graphMapGender: genderCount as JSON])
    }

    def districtWiseEmployee() {
        def resultList = []
        def districtList = ["Karimganj", "Darrang", "Dibrugarh", "Morigaon", "Tinsukia", "Bongaigaon", "Nalbari", "Kokrajhar", "Kamrup", "Karbi-Anglong", "Nagaon", "N.C.Hills", "Dhemaji", "Hailakandi", "Lakhimpur", "Sonitpur", "Dhubri", "Goalpara", "Barpeta", "Golaghat", "Jorhat", "Sibsagar", "Cachar"]
        districtList.each {
            def tempList = []
            def distCount = Worker.countByWorkDistrict(it)
            tempList << it
            tempList << distCount
            resultList << tempList
        }
        redirect(controller: "employeeQRGenerate", action: "report", params: [graphMapDistrict: resultList as JSON])
    }

    def ageCalculate(def birthDate) {
        def resultMap = [:]
        int years = 0;
        int months = 0;
        int days = 0;
        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        //Get difference between months
        months = currMonth - birthMonth;
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        return years
    }

    def assignScheme() {
        def socialSecurityList = SocialSecurity.list()
        def workerList = Worker.list()
        Random picker = new Random();
        workerList.each {
            def rendm = picker.nextInt(socialSecurityList.size())
            if (rendm > 0) {
                for (def i = 0; i < rendm; i++) {
                    def workerSocialSecInst = new WorkerSocialSecurity()
                    def scheme = socialSecurityList[picker.nextInt(socialSecurityList.size())]
                    if (!WorkerSocialSecurity.findBySocialSecurityAndWorker(scheme, it)) {
                        workerSocialSecInst.worker = it
                        workerSocialSecInst.socialSecurity = scheme
                        workerSocialSecInst.save(flush: true)
                    }
                }
            } else {
                def workerSocialSecInst = new WorkerSocialSecurity()
                workerSocialSecInst.worker = it
                workerSocialSecInst.socialSecurity = socialSecurityList[picker.nextInt(socialSecurityList.size())]
                workerSocialSecInst.save(flush: true)
            }

        }
        render("Done")
    }

    def loadWorkerSocialSecurityDetails() {
        def workerInst = Worker.findById(Long.parseLong(params.id))
        def socialSecList = WorkerSocialSecurity.findAllByWorker(workerInst)
        def picture = workerInst.folderYear + "/" + workerInst.folderNumber + "/" + workerInst.workerImage
        [studentInstance: workerInst, socialSecList: socialSecList, picture: picture]
    }

    def loadSocialSecurityDetails() {
        def socialSecInst = SocialSecurity.findById(Long.parseLong(params.socialSecurityId))
        def resultMap = [:]
        resultMap.name = socialSecInst.name
        resultMap.monthlyAmount = socialSecInst.monthlyAmount
        resultMap.fyAmount = socialSecInst.monthlyAmount * 12
        render resultMap as JSON
    }

    def assignFYAmount() {
        Random r = new Random();
        int Low = 5000;
        int High = 20000;

        def socialSecList = WorkerSocialSecurity.list()
        socialSecList.each {
            int R = r.nextInt(High - Low) + Low;
            it.totalPaid = R
            it.save(flush: true)
        }
    }

    def schemeWiseReport() {
        def schemeList = SocialSecurity.list()
        def schemeNameList = [], amountList = [], total = 0
        schemeList.each {
            schemeNameList << it
            def iden = it
            def c = WorkerSocialSecurity.createCriteria()
            def amount = c.get {
                eq('socialSecurity', iden)
                projections {
                    sum("totalPaid")
                }
            }
            total += amount
            amountList << amount
        }
        [schemeNameList: schemeNameList, amountList: amountList, total: total]

    }

    def districtWiseReport() {
        def amountList = [], total = 0
        def districtList = ["Karimganj", "Darrang", "Dibrugarh", "Morigaon", "Tinsukia", "Bongaigaon", "Nalbari", "Kokrajhar", "Kamrup", "Karbi-Anglong", "Nagaon", "N.C.Hills", "Dhemaji", "Hailakandi", "Lakhimpur", "Sonitpur", "Dhubri", "Goalpara", "Barpeta", "Golaghat", "Jorhat", "Sibsagar", "Cachar"]
        districtList.each {
            def workerList = Worker.findAllByWorkDistrict(it)
            def c = WorkerSocialSecurity.createCriteria()
            def amount = 0
            if (workerList.size() > 0) {
                amount = c.get {
                    'in'("worker", workerList)
                    projections {
                        sum("totalPaid")
                    }
                }
            }
            amountList << amount
            total += amount
        }
        [districtList: districtList, amountList: amountList, total: total]

    }

    def loadSocialSecurityDetailsOfADistrict() {
        def amountList = []
        def schemeList = SocialSecurity.list()
        def workerList = Worker.findAllByWorkDistrict(params.district)
        def schemeNameList = [], total = 0
        schemeList.each {
            schemeNameList << it
            def iden = it
            def c = WorkerSocialSecurity.createCriteria()
            def amount = c.get {
                eq('socialSecurity', iden)
                'in'("worker", workerList)
                projections {
                    sum("totalPaid")
                }
            }
            amountList << amount
        }

        def resultMap = [:]
        resultMap.schemeNameList = schemeNameList
        resultMap.amountList = amountList
        render resultMap as JSON
    }

    def loadDistrictsOfASocialSecurityDetails() {
        def amountList = []
        def socialSecInst = SocialSecurity.findById(Long.parseLong(params.socialSecurityId))
        def districtList = ["Karimganj", "Darrang", "Dibrugarh", "Morigaon", "Tinsukia", "Bongaigaon", "Nalbari", "Kokrajhar", "Kamrup", "Karbi-Anglong", "Nagaon", "N.C.Hills", "Dhemaji", "Hailakandi", "Lakhimpur", "Sonitpur", "Dhubri", "Goalpara", "Barpeta", "Golaghat", "Jorhat", "Sibsagar", "Cachar"]
        districtList.each {
            def workerList = Worker.findAllByWorkDistrict(it)
            def c = WorkerSocialSecurity.createCriteria()
            def amount = c.get {
                eq('socialSecurity', socialSecInst)
                'in'("worker", workerList)
                projections {
                    sum("totalPaid")
                }
            }
            amountList << amount
        }

        def resultMap = [:]
        resultMap.socialSecName = socialSecInst.name
        resultMap.districtList = districtList
        resultMap.amountList = amountList
        render resultMap as JSON
    }
}
