package labourdepartmentassam

import fordemo.SocialSecurity
import fordemo.Worker
import fordemo.WorkerSocialSecurity
import grails.transaction.Transactional
import groovy.transform.Synchronized
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.codehaus.groovy.grails.web.context.ServletContextHolder

import java.nio.file.Paths
import java.security.SecureRandom
import java.text.DateFormat
import java.text.SimpleDateFormat

@Transactional
class RegistrationService {
    def springSecurityService
    def springSecurityUtils
    private final myLock = new Object()

    @Synchronized("myLock")
    def saveNewRegistration(params, photographe) {
        def progName = 75
        File inputWorkbook
        String ext = null, path
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        def startYear = 2014
        def studentRegistration
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        if (params.studentId) {
            studentRegistration = Worker.findById(Long.parseLong(params.studentId))
            if (photographe) {
                String fileName = photographe.originalFilename
                def docDirectory = ServletContextHolder.servletContext.getRealPath(System.getProperty("file.separator"))
                def dir = new File(docDirectory + System.getProperty("file.separator") + "studentImage" + System.getProperty("file.separator") + "" + studentRegistration.folderYear + "" + System.getProperty("file.separator") + "" + studentRegistration.folderNumber + "" + System.getProperty("file.separator"))
                dir.mkdirs()
                path = ServletContextHolder.servletContext.getRealPath(System.getProperty("file.separator") + "studentImage" + System.getProperty("file.separator") + "" + studentRegistration.folderYear + "" + System.getProperty("file.separator") + "" + studentRegistration.folderNumber + "" + System.getProperty("file.separator"))
                File inputWorkbook1 = new File(path + "/" + fileName)
                if (!inputWorkbook1.exists()) {
                    photographe.transferTo(new File(path + "/" + fileName))
                } else {
                    if (fileName) {
                        photographe.transferTo(new File(path + "/" + fileName))
                    }
                }
                inputWorkbook = new File(path + "/" + fileName)
                ext = FilenameUtils.getExtension(path + "/" + fileName);
            }
            studentRegistration.firstName = params.firstName
            studentRegistration.lastName = params.lastName
            studentRegistration.middleName = params.middleName
            studentRegistration.gender = params.gender
            studentRegistration.category = params.category
            studentRegistration.mobileNo = Long.parseLong(params.mobileNo)
            studentRegistration.nationality = params.nationality
            studentRegistration.state = params.state
            studentRegistration.address = params.studentAddress
            studentRegistration.addressCity=params.addressTown
            studentRegistration.addressState = params.addressState
            studentRegistration.addressPinCode = params.addressPinCode
            studentRegistration.addressPO = params.addressPO
            studentRegistration.workDistrict = params.workDistrict
            studentRegistration.addressDistrict = params.addressDistrict
            if(params.socialSecurity){
                def workerSocialInst=WorkerSocialSecurity.findAllByWorker(studentRegistration)
                workerSocialInst.each{
                    it.delete(flush: true)
                }
                if(params.socialSecurity.getClass()==String){
                    def newWorkerSocialInst= new WorkerSocialSecurity()
                    newWorkerSocialInst.socialSecurity=SocialSecurity.findById(Long.parseLong(params.socialSecurity))
                    newWorkerSocialInst.worker=studentRegistration
                    newWorkerSocialInst.save(flush: true)
                }
                else{
                    params.socialSecurity.each{
                        def newWorkerSocialInst= new WorkerSocialSecurity()
                        newWorkerSocialInst.socialSecurity=SocialSecurity.findById(Long.parseLong(it))
                        newWorkerSocialInst.worker=studentRegistration
                        newWorkerSocialInst.save(flush: true)
                    }

                }
            }
        } else {
            studentRegistration = new Worker(params)
            if (photographe) {
                String fileName = photographe.originalFilename
                def docDirectory = ServletContextHolder.servletContext.getRealPath(System.getProperty("file.separator"))
                def dir = new File(docDirectory + System.getProperty("file.separator") + "studentImage" + System.getProperty("file.separator") + "" + startYear + "" + System.getProperty("file.separator") + "" + progName + "" + System.getProperty("file.separator"))
                dir.mkdirs()
                path = ServletContextHolder.servletContext.getRealPath(System.getProperty("file.separator") + "studentImage" + System.getProperty("file.separator") + "" + startYear + "" + System.getProperty("file.separator") + "" + progName + "" + System.getProperty("file.separator"))
                File inputWorkbook1 = new File(path + "/" + fileName)
                if (!inputWorkbook1.exists()) {
                    photographe.transferTo(new File(path + "/" + fileName))
                } else {
                    if (fileName) {
                        photographe.transferTo(new File(path + "/" + fileName))
                    }
                }
                inputWorkbook = new File(path + "/" + fileName)
                ext = FilenameUtils.getExtension(path + "/" + fileName);
            }
            studentRegistration.registrationYear = 2014
            def rollNo = getStudentRollNumber(params)
            studentRegistration.identificationNo = rollNo
            studentRegistration.dob = df.parse(params.d_o_b)
            studentRegistration.address = params.studentAddress
            studentRegistration.workDistrict = params.workDistrict
            studentRegistration.folderNumber = "75"
            studentRegistration.folderYear = "2014"
            studentRegistration.addressCity=params.addressTown
        }
        if (ext) {
            inputWorkbook.renameTo(path + "/" + studentRegistration.identificationNo + "." + ext)
            studentRegistration.workerImage =studentRegistration.identificationNo+"."+ext
        }
        if (studentRegistration.save(flush: true, failOnError: true)) {
            return studentRegistration
        } else {
            return null
        }
    }

    def synchronized getStudentRollNumber(params) {
        def status = false
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy"); // Just the year
            int year = 2014
            String courseCodeStr = "75"
            String yearCode = year.toString().substring(2, 4)
            int rollNo = 1
            String rollTemp = null
            int rollTemp1 = 0
            String rollStr = Integer.toString(rollNo)

            if (courseCodeStr.length() > 2) {
                courseCodeStr = courseCodeStr.substring(courseCodeStr.length() - 2, courseCodeStr.length())
            }
            String rollNumber = null;
            def student = Worker.count()
            if (student > 0) {
                def obj = Worker.createCriteria()
                def studentByYearAndCourse = obj.list {
                    eq('folderNumber', "75")
                    and {
                        eq('folderYear', "2014")
                    }
                    maxResults(1)
                    order("identificationNo", "desc")
                }
                if (studentByYearAndCourse.size() > 0) {
                    if (studentByYearAndCourse.get(0).identificationNo) {
                        rollTemp = studentByYearAndCourse.get(0).identificationNo.substring(4, 8)
                        rollTemp1 = Integer.parseInt(rollTemp) + 1
                        rollNumber = courseCodeStr + yearCode + this.prepareSequenceForRollNo(rollTemp1.toString())
                    } else {
                        rollNumber = courseCodeStr + yearCode + this.prepareSequenceForRollNo(rollStr)
                    }
                } else {
                    rollNumber = courseCodeStr + yearCode + this.prepareSequenceForRollNo(rollStr)
                }
            } else {
                rollNumber = courseCodeStr + yearCode + this.prepareSequenceForRollNo(rollStr)
            }
            println("--------#####################################------------" + rollNumber)
            return rollNumber
        } catch (Exception e) {
            println("Problem in roll number generation" + e.printStackTrace())
        }
    }

    private String prepareSequenceForRollNo(String serial) {
        int length
        String rollNoSr
        length = serial.toString().length()
        switch (length) {
            case 1:
                rollNoSr = "000" + serial.toString()
                break;
            case 2:
                rollNoSr = "00" + serial.toString()
                break;
            case 3:
                rollNoSr = "0" + serial.toString()
                break;
            default:
                rollNoSr = serial.toString()
        }
        return rollNoSr

    }
}
