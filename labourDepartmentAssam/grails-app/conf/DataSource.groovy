dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            driverClassName = "com.mysql.jdbc.Driver"
//            url = "jdbc:mysql://localhost:3306/labourDept_Dev?zeroDateTimeBehavior=convertToNull&useUnicode=true&autoreconnect=true"
//            username = "root"
//            password = "root"
            url = "jdbc:mysql://127.5.4.130:3306/labour?zeroDateTimeBehavior=convertToNull&useUnicode=true&autoreconnect=true"
            username = "adminHqEZYb2"
            password = "IC-771asTiRj"
            properties {
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            driverClassName = "com.mysql.jdbc.Driver"
            url = "jdbc:mysql://127.10.16.2:3306/labour?zeroDateTimeBehavior=convertToNull&useUnicode=true&autoreconnect=true"
            username = "adminTCjVRzA"
            password = "7YHJmUl-cmEZ"
            properties {
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
}
