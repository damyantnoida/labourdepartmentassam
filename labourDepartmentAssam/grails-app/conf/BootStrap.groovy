import authentication.Role
import authentication.User
import authentication.UserRole
import fordemo.SocialSecurity

class BootStrap {

    def init = { servletContext ->
        def adminRole =Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN').save(failOnError: true)
        def userRole =Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER').save(failOnError: true)
        def adminUser = User.findByUsername('admin') ?: new User(
                username: 'admin',
                password: 'admin',
                enabled: true).save(failOnError: true)

        if (!adminUser.authorities.contains(adminRole)) {
            UserRole.create adminUser, adminRole
        }
        def normalUser = User.findByUsername('user') ?: new User(
                username: 'user',
                password: 'admin',
                enabled: true).save(failOnError: true)

        if (!normalUser.authorities.contains(userRole)) {
            UserRole.create normalUser, userRole
        }
//        new SocialSecurity(name: 'Rajiv Gandhi Shramik Kalyan Yojana').save(failOnError: true)
//        new SocialSecurity(name: 'Atal Pension Yojana').save(failOnError: true)
//        new SocialSecurity(name: 'Indira Gandhi National Disability Pension Scheme').save(failOnError: true)
//        new SocialSecurity(name: 'National Family Benefit Scheme').save(failOnError: true)
//        new SocialSecurity(name: 'Annapurna Scheme').save(failOnError: true)
//        new SocialSecurity(name: 'Special Provident Funds').save(failOnError: true)
//        new SocialSecurity(name: 'State level social assistance').save(failOnError: true)

    }
    def destroy = {
    }
}
