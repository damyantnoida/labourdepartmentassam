package fordemo

class SocialSecurity {
    String name
    String description
    int monthlyAmount
    static constraints = {
        description(nullable: true)
        monthlyAmount(nullable: true)
    }
    static mapping = {
        name column: "name"
        description column: "description"
        monthlyAmount column: "monthlyAmount"
        monthlyAmount defaultValue: 500
    }
}
