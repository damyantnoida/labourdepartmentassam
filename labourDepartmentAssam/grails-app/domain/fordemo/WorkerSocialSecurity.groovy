package fordemo

class WorkerSocialSecurity {
    Worker worker
    SocialSecurity socialSecurity
    int totalPaid
    static constraints = {
        worker(nullable: true)
        socialSecurity(nullable: true)
        totalPaid(nullable: true)
    }
    static mapping = {
        socialSecurity column: "socialSecurity"
        worker column: "worker"
        totalPaid column: "totalPaid"
        totalPaid defaultValue: 0

    }
}
