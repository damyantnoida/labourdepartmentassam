package fordemo

class Worker {
    String firstName
    String lastName
    String middleName
    Date dob
    String registrationYear
    BigInteger mobileNo
    String gender
    String category
    String nationality
    String state
    String address
    String addressCity
    String addressDistrict
    String addressState
    String addressPO
    String folderNumber
    String addressPinCode
    String workDistrict
    String folderYear
    String workerImage
    String identificationNo
    static constraints = {
        middleName(nullable: true)
        folderYear(nullable: true)
        dob(nullable: true)
        mobileNo(nullable: true)
        folderNumber(nullable: true)
        gender(nullable: true)
        addressDistrict(nullable: true)
        addressState(nullable: true)
        addressCity(nullable: true)
        addressPO(nullable: true)
        addressPinCode(nullable:true)
        workerImage(nullable: true)
        workDistrict(nullable: true)
        registrationYear(nullable: true)
    }
    static mapping ={
        registrationYear column: "registrationYear"
        firstName column: "firstName"
        folderYear column: "folderYear"
        folderNumber column: "folderNumber"
        lastName column: "lastName"
        middleName column: "middleName"
        mobileNo column: "mobileNo"
        dob column: "Dob",index: 'Dob_Index'
        gender column: "Gender"
        addressDistrict column: "AddressDistrict"
        addressState column: "AddressState"
        addressPinCode column: "AddressPinCode"
        address column: "address"
        addressCity column: "addressCity"
        registrationYear column: "RegistrationYear"
        workDistrict column: "workDistrict"
        workerImage column: "workerImage"
        identificationNo column: "identificationNo"
    }
}
